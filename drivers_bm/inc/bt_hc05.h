 /*
 Copyright 2018 Joaquin Furios
 
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
 
 #ifndef BT_HC05_H
 #define BT_HC05_H
 /** @addtogroup BT HC05 (zs-040)
 *  @{
 *  @file bt_hc05.h
 *  @brief Basic driver for the use of the HC-05 bluetooth module integrated in the ZS-040 board.
 *
 *  @note For the correct operation of this driver it is necessary to use the UART driver (Spanish version) made by Sebastian Mateos
 *
 * Modification history (new versions first)
 * -----------------------------------------------------------
 *   Date   | Version |   Description   | Initial
 * :-------:|:-------:|:---------------:|:--------
 * 2018/12/04| v1.0 | First full version|   JF
 * 2018/11/20| v0.1 | Initial version   |   JF
 *
 *-------------------------------------------------------------
 *   Initials  |     Name
 * :----------:|:----------------
 *      JF     | Joaquin Furios 
 *
 *
 *  @date 20/11/2018
 */
 

/*==================[inclusions]=============================================*/
#include <stdint.h>


/*==================[external functions definition]==========================*/
/** @fn void btInit(void)
 * @brief Initializes the RS232_UART and USB_UART for the operation of the bluetooth module.
 * @note The default baud rate of the HC-05 is 9600
 */
void btInit (void);

/** @fn btActivateTransparentModUsb (void)
 * @brief Everything received via bluetooth will be sent by the USB_UART and
 *  everything received via USB_UART will be transmitted by bluetooth.
 * @note The default baud rate usb is 115200.
 */
void btActivateTransparentModUsb (void);

/** @fn btSendByte (uint8_t* byte)
 * @brief Send a byte via bluetooth
 * @param byte Pointer to the byte you want to send.
 */
void btSendByte (uint8_t* byte);

/** @fn btSendString (uint8_t* msg)
 * @brief Send a string via bluetooth
 * @param msg Pointer to the beginning of the string.
 */
void btSendString (uint8_t* msg);

/** @fn btReadByte (uint8_t* byte)
 * @brief Read the first byte received via bluetooth.
 * @param byte Pointer where the received byte is stored.
 */
void btReadByte (uint8_t* byte);

/** @fn btReadString (uint8_t* msg)
 * @brief Read the string received via bluetooth.
 * @param msg Pointer to the first element of the received string.
 */
void btReadString (uint8_t* msg);

/** @fn btChangeName (uint8_t* name)
 * @brief Change the name of the module.
 * @param name Pointer to the new name.
 * @note Need initialize bluetooth module in AT mode.
 */
void btChangeName (uint8_t* name);

/** @fn btChangePass (uint8_t* pass)
 * @brief Change the password of the module.
 * @param pass Pointer to the new password.
 * @note Need initialize bluetooth module in AT mode.
 */
void btChangePass (uint8_t* pass);

/** @fn btChangeBaudRae (uint8_t* baud)
 * @brief Change the baud rate of the module.
 * @param baud Pointer to the new baud rate.
 * @note Need initialize bluetooth module in AT mode.
 */
void btChangeBaudRae (uint8_t* baud);

/** @fn btReadName (uint8_t *name)
 * @brief Returns the module name.
 * @param name Pointer where the response of the module is returned.
 * @note Need initialize bluetooth module in AT mode.
 */
void btReadName (uint8_t* name);

/** @fn btReadPass (uint8_t *pass)
 * @brief Returns the module password.
 * @param pass Pointer where the response of the module is returned.
 * @note Need initialize bluetooth module in AT mode.
 */
void btReadPass (uint8_t* pass);

/** @fn btReadBaudRate (uint8_t* baud)
 * @brief Returns the baud rate of the module.
 * @param baud Pointer where the response of the module is returned.
 * @note Need initialize bluetooth module in AT mode.
 */
void btReadBaudRate (uint8_t* baud);

/*==================[end of file]============================================*/
#endif // #ifndef BT_HC05_H
 
 /** @}*/
