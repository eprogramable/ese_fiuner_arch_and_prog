/* @brief  EDU-CIAA NXP PN532 driver
 * @author Juan Ignacio Cerrudo
 *
 * This driver provide functions to configure and handle a PN532 NFC module
 *
 * @note
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 23/11/2018 | Document creation		                         |
 *
 */
#ifndef INC_PN532_H_
#define INC_PN532_H_

#include "sapi_datatypes.h"

//#define PN532DEBUG 1

/*If define PN532_P2P_DEBUG, all the data that initiator and target received */
/*in the peer to peer communication, will be printed in the serial port tool window*/
//#define PN532_P2P_DEBUG 1

#define PN532_PREAMBLE 0x00
#define PN532_STARTCODE1 0x00
#define PN532_STARTCODE2 0xFF
#define PN532_POSTAMBLE 0x00

#define PN532_HOSTTOPN532 0xD4

#define PN532_FIRMWAREVERSION 0x02
#define PN532_GETGENERALSTATUS 0x04
#define PN532_SAMCONFIGURATION  0x14
#define PN532_INLISTPASSIVETARGET 0x4A
#define PN532_INDATAEXCHANGE 0x40
#define PN532_INJUMPFORDEP 0x56
#define PN532_TGINITASTARGET 0x8C
#define PN532_TGGETDATA 0x86
#define PN532_TGSETDATA 0x8E

#define PN532_MIFARE_READ 0x30
#define PN532_MIFARE_WRITE 0xA0

#define PN532_AUTH_WITH_KEYA 0x60
#define PN532_AUTH_WITH_KEYB 0x61


#define PN532_WAKEUP 0x55

#define  PN532_SPI_STATREAD  0x02
#define  PN532_SPI_DATAWRITE 0x01
#define  PN532_SPI_DATAREAD  0x03

#define  PN532_SPI_READY  0x01

#define PN532_MIFARE_ISO14443A 0x0

#define KEY_A	1
#define KEY_B	2

#define PN532_BAUDRATE_201K 1
#define PN532_BAUDRATE_424K 2


/**************************************************************************/
  /*!
      Init PN532 device
      @param  port 		SPI port to use
      @param  spi_br    SPI communication speed
      @param  spi_cs    GPIO pin to use as Chip Select

  */
  /**************************************************************************/
  void PN532Init(uint8_t port, uint32_t spi_br, uint8_t spi_cs);

  /**************************************************************************/
  /*!
      @brief  Configures the SAM (Secure Access Module)
  */
  /**************************************************************************/
  bool_t PN532SAMConfig(void);

  /**************************************************************************/
  /*!
      @brief  Checks the firmware version of the PN5xx chip
      @returns  The chip's firmware version and ID
  */
  /**************************************************************************/
  uint32_t PN532GetFirmwareVersion(void);

  /**************************************************************************/
  /*!
      Waits for an ISO14443A target to enter the field
      @param  cardBaudRate  Baud rate of the card
      @param  uid           Pointer to the array that will be populated
                            with the card's UID (up to 7 bytes)
      @param  uidLength     Pointer to the variable that will hold the
                            length of the card's UID.
      @returns 1 if everything executed properly, 0 for an error
  */
  /**************************************************************************/
  uint32_t PN532ReadPassiveTargetID(uint8_t cardbaudrate);

  /**************************************************************************/
  /*!
      Tries to authenticate a block of memory on a MIFARE card using the
      INDATAEXCHANGE command.  See section 7.3.8 of the PN532 User Manual
      for more information on sending MIFARE and other commands.
      @param  uid           Pointer to a byte array containing the card UID
      @param  block_address   The block number to authenticate.  (0..63 for
                            1KB cards, and 0..255 for 4KB cards).
      @param  authtype     Which key type to use during authentication
                            (0 = MIFARE_CMD_AUTH_A, 1 = MIFARE_CMD_AUTH_B)
      @param  keys       Pointer to a byte array containing the 6 byte
                            key value
      @returns 1 if everything executed properly, 0 for an error
  */
  /**************************************************************************/
  uint32_t PN532AuthenticateBlock(	uint8_t cardnumber /*1 or 2*/,
  uint32_t cid /*Card NUID*/,
  uint8_t block_address /*0 to 63*/,
  uint8_t authtype /*Either KEY_A or KEY_B */,
  uint8_t* keys);

  /**************************************************************************/
  /*!
      Tries to read an entire 16-byte data block at the specified block
      address.
      @param  block_address   The block number to authenticate.  (0..63 for
                            1KB cards, and 0..255 for 4KB cards).
      @param  data          Pointer to the byte array that will hold the
                            retrieved data (if any)
      @returns 1 if everything executed properly, 0 for an error
  */
  /**************************************************************************/
  bool_t PN532ReadMemoryBlock(uint8_t card_number ,uint8_t block_address , uint8_t * data);

  /**************************************************************************/
  /*!
      Tries to write an entire 16-byte data block at the specified block
      address.
      @param  block_address   The block number to authenticate.  (0..63 for
                            1KB cards, and 0..255 for 4KB cards).
      @param  data          The byte array that contains the data to write.
      @returns 1 if everything executed properly, 0 for an error
  */
  /**************************************************************************/
  bool_t PN532WriteMemoryBlock(uint8_t card_number /*1 or 2*/,uint8_t block_address /*0 to 63*/, uint8_t * data);

  /**********************************************************************/
  /*!
    Configure the NFC shield as initiator in the peer to peer
   	      commnunication and only the initiator set the baud rate.
  	  @param :-uint8_t baudrate,Any number from 0-2. 0 for 106kbps or 1 for 201kbps or 2 for 424kbps but 106kps is not supported yet;
  	  @returns boolean,true = the shield finds the target and is configured as initiator successfully.
  */
  /**************************************************************************/

  uint32_t PN532ConfigurePeerAsInitiator(uint8_t baudrate /* Any number from 0-2. 0 for 106kbps or 1 for 201kbps or 2 for 424kbps */); //106kps is not supported

  /**********************************************************************/
   /*!
    	This method implements a Peer to Peer Target.
   	  @returns boolean,true = when successful.
   */
   /**************************************************************************/
  uint32_t PN532ConfigurePeerAsTarget();

  /**************************************************************************/
    /*!
        This method is used to transmit and receive data to and from target.
        This code is used by NFC Peer to Peer Initiator.
        @param  dataOut is pointer and array of chars (16 bytes) transmit data.
        @param  dataIn is pointer and array of chars (16 bytes) receive data.
        @returns boolean,true = when successful.
    */
    /**************************************************************************/
  bool_t PN532InitiatorTxRx(char* dataOut,char* dataIn);

  /**************************************************************************/
      /*!
          This method is used to transmit and receive data to and from initiator.
          This code is used by NFC Peer to Peer Target to respond to Initiator
          @param  dataOut is pointer and array of chars (16 bytes) transmit data.
          @param  dataIn is pointer and array of chars (16 bytes) receive data.
          @returns boolean,true = when successful.
      */
      /**************************************************************************/
  uint32_t PN532TargetTxRx(char* dataOut,char* dataIn);

  /**************************************************************************/
  /*!
      @brief  Sends a command and waits a specified period for the ACK
      @param  cmd       Pointer to the command buffer
      @param  cmdlen    The size of the command in bytes
      @param  timeout   timeout before giving up
      @returns  1 if everything is OK, 0 if timeout occured before an
                ACK was recieved
  */
  /**************************************************************************/
  bool_t PN532SendCommandCheckAck(uint8_t *cmd, uint8_t cmdlen, uint16_t timeout);

  /**************************************************************************/
  /*!
      @brief  Low-level SPI write wrapper
      @param  c       8-bit command to write to the SPI bus
  */
  /**************************************************************************/
  void PN532Write(uint8_t _data);
  /**************************************************************************/
  /*!
      @brief  Low-level SPI read wrapper
      @returns The 8-bit value that was read from the SPI bus
  */
  /**************************************************************************/
  uint8_t PN532Read(void);

  /**************************************************************************/
  /*!
      @brief  Return true if the PN532 is ready with a response.
  */
  /**************************************************************************/
  uint8_t PN532ReadSpiStatus(void);

  /**************************************************************************/
  /*!
      @brief  Tries to read the SPI or I2C ACK signal
  */
  /**************************************************************************/
  bool_t PN532CheckSpiAck();

  /**************************************************************************/
  /*!
      @brief  Reads n bytes of data from the PN532 via SPI or I2C.
      @param  buff      Pointer to the buffer where data will be written
      @param  n         Number of bytes to be read
  */
  /**************************************************************************/
  void PN532ReadBytes(uint8_t* buff, uint8_t n);

  /**************************************************************************/
  /*!
      @brief  Writes a command to the PN532, automatically inserting the
              preamble and required frame details (checksum, len, etc.)
      @param  cmd       Pointer to the command buffer
      @param  cmdlen    Command length in bytes
  */
  /**************************************************************************/
  void PN532WriteCommand(uint8_t* cmd, uint8_t cmdlen);

#endif /* INC_PN532_H_ */
