/** @addtogroup SHT71x
 *  @{
 *  @file sht71x.h
 *  @brief Driver to use with SHT71x
 *  @date 10/11/2018
 */
#include <stdint.h>

#ifndef MODULES_LPC4337_M4_DRIVERS_BM_INC_SHT71X_H_
#define MODULES_LPC4337_M4_DRIVERS_BM_INC_SHT71X_H_

#define MEASURE_BLOCKING		0	/**<Wait for measure to be ready*/
#define MEASURE_NOBLOCKING		1	/**<Don't wait for measure to be ready*/

/**@brief Struct pinInfo
 *
 * This structure define de PIN and GPIO
 *
 */
typedef struct{
	uint8_t	port;				/**<Port asociated to PIN*/
	uint8_t	pin;				/**<Corresponding PIN*/
	uint8_t	gpioPort;			/**<Port asociated to GPIO*/
	uint8_t	gpioPin;			/**<GPIO*/
} _pinInfo;


/**
 * @brief Initialization
 * Configure the pins going to use for DATA and SCK.
 *
 * By default we use GPIO5 and GPIO6 on the EDU-CIIA
 *
 */
void SHT71xConfigurePins(_pinInfo apinDATA, _pinInfo apinSCK);

/**
 * @brief Initialization
 *
 * Configure the pins going to use for DATA and SCK. These pins must to be selected
 * using the definitions.
 *
 */
void SHT71xInit();


/**
 * @brief Hardware Reset
 * Reset communication.
 */
void SHT71xConnectionReset(void);

/**
 * @brief Soft Reset
 * Reset the sensor by a soft reset.
 * @return Return 1 in case of no response from sensor.
 */
uint8_t SHT71xSoftReset(void);


/**
 * @brief Write STATUS
 *
 * Perform a new configuration write the STATUS register.
 *
 * @param [in] value: Byte with the new configuration.
 */
uint8_t SHT71xWriteStatus(uint8_t value);


/**
 * @brief Read STATUS
 *
 * Read the STAUS register with the checksum.
 *
 * @param [in] status: Pointer where receive the STATUS value.
 * @param [in] checksum: Pointer where receive the CHECKSUM value.
 *
 * @return Return 1 in case of no response from sensor.
 *
 */
uint8_t SHT71xReadStatus(uint8_t *status, uint8_t *checksum);


/**
 * @brief Measure Temperature.
 *
 * Start a new measure for the temperature.
 *
 * @param[in] tempValue: Pointer where to receive the TEMP value.
 * @param[in] checksum: Pointer where to receive the CHECKSUM value.
 * @param[in] mode: mode = 0 Read the temperature and wait until the sensor finishes or for a timeout of 1 second.
 * mode = 1 start the measurement and return without error, in this mode you must check if the measurement is ready.
 * @return Return 1 in case of no response from sensor.
 *
 */
uint8_t SHT71xMeasureTemp(uint16_t *tempValue, uint8_t *checksum, uint8_t mode);

/**
 * @brief Measure Humidity.
 *
 * Start a new measure for the humidity.
 *
 * @param[in] rhValue: Pointer where to receive the RH value.
 * @param[in] checksum: Pointer where to receive the CHECKSUM value.
 * @param[in] mode: mode = 0 Read RH and wait until sensor finishes or for a
 * timeout of 1 second. mode = 1 start the measurement and return without error, in this mode
 * you must to check if the measure is ready.
 * @return Return 1 in case of no response from sensor.
 *
 */
uint8_t SHT71xMeasureRH(uint16_t *rhValue, uint8_t *checksum, uint8_t mode);

/**
 * @brief Calculate the temperature in Centigrade.
 *
 * @param[in] tempValue: Value read from sensor.
 * @return Return the temperature read by sensor in centigrades.
 *
 */
float SHT71xCalcCentigrade(uint16_t tempValue);

/**
 * @brief Calculate the temperature in Farenheight.
 *
 * @param[in] tempValue: Value read from sensor.
 * @return Return the temperature read by sensor in Farenheights.
 *
 */
float SHT71xCalcFarenheight(uint16_t tempValue);

/**
 * @brief Calculate the Relative Humidity.
 *
 * @param[in] rhValue: Value read from sensor.
 * @param[in] tempValue: Value of the current temperature in centigrades.
 * @return Return the true relative humidity.
 *
 */
float SHT71xCalcRH(uint16_t rhValue, float tempValue);

/**
 * @brief Calculate the Dew Point.
 *
 * @param[in] rhValue: Current value of relative humidity.
 * @param[in] tempValue: Current value of temperature in centigrades.
 * @return Return the dew point.
 *
 */
float SHT71xCalcDewPoint(float rhValue, float tempValue);




#endif /* MODULES_LPC4337_M4_DRIVERS_BM_INC_SHT71X_H_ */

/** @}*/
