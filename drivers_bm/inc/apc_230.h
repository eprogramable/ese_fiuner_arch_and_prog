/*
 * apc_230.h
 *
 *  Created on: 10 nov. 2018
 *      Author: pola
 */

#ifndef MODULES_LPC4337_M4_DRIVERS_BM_INC_APC_230_H_
#define MODULES_LPC4337_M4_DRIVERS_BM_INC_APC_230_H_
#include <stdint.h>
/*==================[external data declaration]==============================*/
/** \brief Definition of constants to reference the APC Baud Rates Configs.
 **
 **/
//#define DIS			UART_LCR_PARITY_DIS
//#define ODD			UART_LCR_PARITY_ODD
//#define EVEN 			UART_LCR_PARITY_EVEN
#define CHIP_USART 	LPC_USART3


//enum BAUD_RATE {BR_1200 ='0', BR_2400='1', _BR_4800='2', BR_9600='3', BR_19200='4', BR_38400='5', BR_57600='6'};
enum BAUD_RATE {BR_1200, BR_2400, BR_4800, BR_9600, BR_19200, BR_38400, BR_57600};
enum SERIES_CHECKOUT {BOOD_DIS, BOOD_ODD, BOOD_EVEN};// Bitde paridad 0:sin paridad 1:pariad par 2:paridad impar
typedef void (*pFunc)(void);

/** \brief Definition of constants to control the EDU-CIAA leds.
 **
 **/
typedef struct {
	char apc_odd;
	uint8_t uart_odd;
}s_parity;

typedef struct {
	unsigned char * apc_baud;
	int	uart_bau;
}s_rate;

typedef struct{

	char frecuency[6];						/// Frecencia de modulacion - desde  418000kHz a 455000KHz
	uint8_t air_rate;						///	Baud Rate transmision de RF
	uint8_t out_power;							/// Potencia transmision en RF -> '0' a '9'
	uint8_t serie_rate;						/// Baud Rate puerto serie transmision RS232
	uint8_t parity;							/// Bit de paridad de transmision RS232
	pFunc Rx;								/// Puntero a Funcion invocada en la llamada a interrupcion OnRx

	} apc_conf;

/*==================[external functions declaration]=========================*/
/** \brief Configuration function of UART3 P2_3(RS232 TXD) P2_4(RS232 RXD) and Pins Module
**
** \Configura la los pines DEL GPIO para UART y pines accesorios para funcionamiemto del APC
**
** \param[in] struct apc_conf
**
** \return TRUE if no error
**/

uint8_t ApcInit(void);				///Configuarcion de pines de la CIA e inicializacion de UART


/** \brief Configuration function of APC 230 Module
**
** \Setea los parametros de configuracion de Frecuencia de transmision, velocidad de transmision en RF, configuracion del puerto serie, potencia de transmision de RF
**
** \param[in] No parameter
**
** \return TRUE if no error
**/

uint8_t ApcConfig(apc_conf * conf);				/// Configuracion del transmisor
//void ApcSetPort(uint8_t port);

/** \brief One Byte Transmit function with APC 230 Module
**
** \Envia un solo Byte al modulo de RF APC 230
**
** \param[in] character to send
**
** \return No parameter
**/
void ApcTx(char dato);						///	Transmite un byte por transmisor

/** \brief Data Buffer Transmit function with APC 230 Module
**
** \Envia una cadena de 'size' Bytes al modulo de RF APC 230
**
** \param[in] puntero a la cadena de caracteres a transmitir, tamaño de la cadena a transmitir
**
** \return No parameter
**/
void ApcTxBuff(const char * buff);		/// Transmite un paquete de 'size' Bytes al transmisor

/** \brief One Byte receive function with APC 230 Module
**
** \Recibe un Bytes desde modulo de RF APC 230
**
** \param[in] No parameter
**
** \return Un caracter
**/
char ApcRx(void);							/// Lee un Byte del Buffer del puerto serie

/** \brief Buffer receive function with APC 230 Module
**
** \Recibe una cadena de  Bytes desde modulo de RF APC 230
**
** \param[in] puntero a cadena de caracteres, tamaño del buffer a leer.
**
** \return Un caracter
**/
void ApcRxBuff(char * buff, int size);

/** \brief On Module function with APC 230 Module
**
** \Enciende el  modulo de RF APC 230
**
** \param[in] No parameter
**
** \return return TRUE if no error
**/
void ApcOn(void);							///	Enciende el modulo RF

/** \brief Off Module function with APC 230 Module
**
** \Apaga el  modulo de RF APC 230
**
** \param[in] No parameter
**
** \return return TRUE if no error
**/
uint8_t ApcIsOn(void);
void ApcOff(void);							/// Apaga el modulo RF
//uint8_t ApcDiscover(void);

void ApcCheck(void);

void ApcConfMode(void);
void ApcRunMode(void);




#endif /* MODULES_LPC4337_M4_DRIVERS_BM_INC_APC_230_H_ */
