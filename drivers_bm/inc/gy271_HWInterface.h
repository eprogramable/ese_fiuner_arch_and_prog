/*
 * gy271_HWInterface.h
 *
 *  Hardware interface for the GY271 deviece, breakout of the HMC5883L magnometer.
 *  Provides I2C and GPIO interfaces for configuring and reading the device, and configures
 *  the GPIO interruptions.
 *
 *
 *  Created on: 6 dic. 2018
 *      Author: esteban
 */

#ifndef GY271_HWINTERFACE_H
#define GY271_HWINTERFACE_H

#include "gy271_magnetometer.h"



/**
 * @brief Port initializing function.
 * Also checks if the I2C port has been initialized by this driver, and configures
 * the GPIO port where the DRDY output is present.
 * @return None
 */
void GY271HWIInitPorts(GY271_magnetometer_t device);

/**
 * @brief Drops the configuration in the device parameters to the breakout.
 * @param GY271_magnetometer_t type device, with the desired configuration.
 * @return Flags with ones if the corresponding register couldn't be read
 *  0 - 0 - 0 - 0 - 0 - Reg. A  - Reg. B  - Mode Reg.
 *
 */
uint8_t gy271HWIConfigDevice(GY271_magnetometer_t GY271_dev);

/**
 * @brief Updates the values readings to the latest availables at the
 * selected device and stores them in the argument.
 * @param GY271_magnetometer_t * GY271_dev pointer to the desired reading device
 * @return uint8_t as flags of 1's in the wrong readings, for the following format:
 *    0 - 0 - X_MSB - X_LSB - Z_MSB -Z_LSB - Y_MSB -Y_LSB
 */
uint8_t GY271HWIReadDevice(GY271_magnetometer_t * GY271_dev);



#endif /* MODULES_LPC4337_M4_DRIVERS_BM_INC_GY271HWINTERFACE_H_ */
