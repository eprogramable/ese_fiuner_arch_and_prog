/*
 * i2cbm.h
 *
 *  Created on: 30 nov. 2018
 *      Author: esteban
 */


#ifndef I2CBM_H_
#define I2CBM_H_

#include "chip.h"

#define I2C_PORT I2C0
#define I2C_DATA_RATE	100000
/**
 * Initializes the I2C port. Admits values enumerated in I2C_ID_T definition.
 */
void I2CInit();

/**
 * Reads from i2c port
 */

uint8_t I2CRead(  uint16_t  i2cSlaveAddress,
				  uint8_t* dataToReadBuffer,
				  uint16_t dataToReadBufferSize,
				  uint8_t* receiveDataBuffer,
				  uint16_t receiveDataBufferSize);

/**
 * writes on i2c port
 */
uint8_t I2CWrite(uint16_t address, uint8_t* data, uint16_t data_size);
//


#endif /* I2CBM_H_ */
