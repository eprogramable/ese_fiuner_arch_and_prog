/* Copyright 2018,
 * Sebastian Mateos
 * sebastianantoniomateos@gmail.com
 * Cátedra Electrónica Programable
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @addtogroup spi
 *  @{
 *  @file spi.h
 *  @brief Driver Bare Metal para la interfaz spi de la EDU_CIAA
 *
 *
 *  @section changeLog Historial de modificaciones (la mas reciente arriba)
 *
 *  20180922 v0.1 version inicial SM
 *
 *  Iniciales  |     Nombre
 * :----------:|:----------------
 *  SM		   | Sebastian Mateos
 *
 *  @date 09/11/2018
 */

#ifndef SPI_H
#define SPI_H

/*==================[inclusions]=============================================*/
#include <stdint.h>
#include "bool.h"

/*==================[macros]=================================================*/
#define BIT_RATE 3000000 /**< Bits por segundo de la SPI*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/
/** @fn void InitSPI(void)
 * @brief Inicializacion de la interfaz SPI
 */
void InitSPI(void);

/** @fn void DeinitSPI(void)
 * @brief Deinicializacion de la interfaz SPI
 */
void DeinitSPI(void);

/** @fn void TransferenciaBufferSPI(uint8_t *tx_buff, uint8_t *rx_buff, uint8_t longitud)
 * @brief
 * @param[in]
 * @param[in]
 * @param[in]
 */
void TransferenciaBufferSPI(uint8_t *tx_buf, uint8_t *rx_buf, uint8_t longitud);

/** @fn uint8_t TransferenciaRapidaSPI(uint8_t tx)
 * @brief
 * @param[in] tx
 * @return
 */
uint8_t TransferenciaRapidaSPI(uint8_t tx);

/*==================[end of file]============================================*/
#endif /* SPI_H */

/** @}*/
