/* # Driver for gy271 magnetometer, a HMC5883L breakout #
 *  \brief: A driver for the GY271 magnetometer, based on the HMC5883L.
 *  The driver is meant to handle up to 4 magnetometers in the GY271_
 *  devices array, given such ammount in the  GY271_MAX_ITEMS definition.
 *  Magnetometers DRDY output should be connected incrementally in the GPIOX ports,
 *  0 \le X \le 3 . Thus, GPIO ports 0 to 3 are reserved when using this driver.
 *  Before initialization, each item should be configured, defining its I2C address
 *  and sampling parameters.
 *  \file: gy271_magnetometer.h
 *  \date 16 nov. 2018
 *  \author: Esteban Osella
 *  \email: eosella@ingenieria.uner.edu.ar
  */

#ifndef GY271_MAGNETOMETER_H
#define GY271_MAGNETOMETER_H


#include <stdint.h>
//#include "gy271_HWInterface.h"
/*===================== [external defnitions] ======================*/
//I2C configuration
#define I2C_PORT I2C0
#define I2C_DATA_RATE 100000

//magnetometer definitions
/**
 * Amount of magnetometers to be handled.
 */
#define GY271_MAX_ITEMS	0x01

/**
 * Control register A configuration 
 * Averaged samples per measurement output.
*/

typedef struct{
	uint8_t gpio_number;
	uint8_t scu_port;
	uint8_t scu_pin;
	uint8_t gpio_port;
	uint8_t gpio_pin;
	uint8_t config_mode;
	uint8_t IRQNumber;
	void * IRQHandler;
} port_t ;


enum {
	GY271_1_SAMPLE ,
	GY271_2_SAMPLE ,
	GY271_4_SAMPLE ,
	GY271_8_SAMPLE ,
	GY271_D_SAMPLE = GY271_1_SAMPLE
} ;

/**
 * Control register A configuration 
 * Data output rate
*/
 enum {
	GY271_0_75HZ ,
	GY271_1_50HZ ,
	GY271_3_00HZ ,
	GY271_7_50HZ ,
	GY271_15_0HZ ,
	GY271_30_0HZ ,
	GY271_75_0HZ ,
	GY271_DEF_HZ = GY271_15_0HZ
} ;

/**
 * Control register A configuration 
 * Measurement configuration bits. These bits define the measurement flow of the device, specifically whether or not to incorporate an appied bias into the measurement.
*/
enum{
	GY271_NORM_MEASUREMENT , //!< The device follows normal measurement flow. The positive and negative pins of the resistive load are left floating and high impedance.
	GY271_PBC_MEASUREMENT  , //!< Positive bias configuration for X,Y, and Z axes. A positive current is forced across the resistive load for all three axes.
	GY271_NBC_MEASUREMENT   //!< Negative bias configuration for X, Y, and Z axes. A negative current is forced across the resistive load for all three axes.
} ;

/** 
 * Control register B configuration
 * Gain configuration bits. 8 gain levels. The new gain setting is effective from the second measurement and on. Use the Gain value to convert from counts to Gauss
*/
enum
{
	GY271_GAIN_088 , //!< Gain: 1370
	GY271_GAIN_130 , //!< Gain: 1090
	GY271_GAIN_190 , //!< Gain: 820
	GY271_GAIN_250 , //!< Gain: 660
	GY271_GAIN_400 , //!< Gain: 440
	GY271_GAIN_470 , //!< Gain: 390
	GY271_GAIN_560 , //!< Gain: 330
	GY271_GAIN_810 , //!< Gain: 230
	GY271_GAIN_DEF =  GY271_GAIN_130
} ;


/**
 * Operation mode
 * It can be continuous, single meausurement, or in idle mode.
 */
enum
{
	GY271_CONTINUOUS_MEASUREMENT, //!< Continuously performs a measurement and places the result in the registers. RDY goes high when a new data is available.
	GY271_SINGLE_MEASUREMENT    , //!< Device performs a single measurement, sets RDY high and returned to idle mode
	GY271_IDLE_MODE             , //!< Device is placed in idle mode
	GY271_DEFAULT_MODE 		     = GY271_SINGLE_MEASUREMENT
} ;


/**
 * Basic configuration structure of the magnetometer.
 */
typedef struct
{
	uint8_t		samples;
	uint8_t 	rate;
	uint8_t 	flow;
	uint8_t 	gain;
	uint8_t 	mode; //operation mode
	uint8_t	GPIO_DRDY;

} GY271_config_t;

/**
 * Defines the structure for each magnetometer,
 */
typedef struct
{
	GY271_config_t 	config;
	uint8_t			base_address;
	uint8_t			write_address;
	uint8_t			read_address;
	int16_t			x_read;
	int16_t			y_read;
	int16_t			z_read;
	int16_t			temperature;
	//port_t 			DRDY_GPIO_port
} GY271_magnetometer_t;

/* ===================[external variables declarations]===========*/
/**
 * Array of available magnetometers to read. This array will be iterated when
 * the reading update is performed in order to synchronize the initializations,
 * readings and update.
 *
 * \Note: A thing that will be needed to keep in mind is that some devices might
 * have different sampling rate, which will potentially introduce errors. The
 * implemented solution to deal with this issue was by forcing every device to
 * operate with the highest selected frequency. Otherwise, a timer should be
 * reserved for each device, which might introduce unnecessary resources
 * consumption.
 */
GY271_magnetometer_t GY271_devices[GY271_MAX_ITEMS];

/*===================[external functions declarations]==================*/



/**
 * Based on the configuration stated in the argument, starts the i2c port
 * and performs the configuration of the magnetometer.
 * \return Numer of configured devices.
 */
uint8_t GY271ConfigDevice(GY271_magnetometer_t GY271_dev);

/**
 * Starts the i2c port and performs a bulk configuration to every device stated
 * in the GY271_devices array
 * \return Number of successfully configured devices.
 */
uint16_t GY271ConfigAll();


/**
 * Updates the readings of the selected magnetometer.
 * \return Flags of register correcltly read
 */
uint8_t GY271ReadDevice(GY271_magnetometer_t * GY271_dev);

/**
 * Forces a read request of every magnetometer in the GY271_devices array
 * and updates the readings there.
 *
 * \return Number of successfully read values.
 */
uint16_t GY271ReadAll();


/**
 * \brief Translate the readings to Gauss units, based on the selected gain.
 * Note that these values are valid starting on the second reading
 * after a gain change.
 * \param GY271_magnetometer_t GY271_dev: the device to get the measures
 * \param float_t * x_read_guass: output converted x measure
 * \param float_t * y_read_guass: output converted z measure
 * \param float_t * z_read_guass: output converted z measure
 */
void GetGaussReadings(GY271_magnetometer_t GY271_dev, float * x_read_guass, float * y_read_guass, float * z_read_guass);

/**
 * Writes a default configuration structure on the argument.
 */
void GY271GetDefaultConfig(GY271_config_t * config);

#endif /* GY271_MAGNETOMETER_H */
