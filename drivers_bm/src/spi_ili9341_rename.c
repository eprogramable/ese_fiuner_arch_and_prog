/* @brief EDU-CIAA NXP SPI driver
 * @author Albano Peñalva
 *
 * This driver provide functions to configure and handle the EDU-CIAA NXP SPI port
 * (named here SPI1) which use the SSP1 module of the LPC4337.
 *
 * @note This is not an SSP driver, therefore the other protocols supported for
 * this module are not implemented, just the SPI mode.
 *
 * @note This driver provide the functions to manage just the SSP1 module because
 * is the only one implemented in the EDU-CIAA NXP. However, it remains compatible
 * for future upgrades.
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/11/2018 | Document creation		                         |
 *
 */

#include "spi.h"
#include "chip.h"

/*****************************************************************************
 * Private macros/types/enumerations/variables definitions
 ****************************************************************************/

#define SUCCESS	1			/* */
#define ERROR 	0			/* */
#define BITS8	8

#define PORT_MISO1	0x1		/*!< SSP1 MISO at pin P1.3 */
#define PIN_MISO1	0x3
#define PORT_MOSI1 	0x1		/*!< SSP1 MOSI at pin P1.4 */
#define PIN_MOSI1 	0x4
#define PORT_SCK1 	0xF		/*!< SSP1 SCK at pin PF.4 */
#define PIN_SCK1 	0x4

uint8_t lsb_first;

/*****************************************************************************
 * Public types/enumerations/variables declarations
 ****************************************************************************/

/*****************************************************************************
 * Private functions definitions
 ****************************************************************************/

/*****************************************************************************
 * Private functions declarations
 ****************************************************************************/

/*****************************************************************************
 * Public functions declarations
 ****************************************************************************/

uint8_t SpiInit(spiConfig_t spi)
{
	uint8_t ret_value;

	switch(spi.port)
	{
	case SPI_1:
		/* Configure SSP1 pins */
		Chip_SCU_PinMuxSet(PORT_SCK1, PIN_SCK1, (SCU_MODE_PULLUP | SCU_MODE_FUNC0));
		Chip_SCU_PinMuxSet(PORT_MISO1, PIN_MISO1, (SCU_MODE_PULLUP | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS	| SCU_MODE_FUNC5));
		Chip_SCU_PinMuxSet(PORT_MOSI1, PIN_MOSI1, (SCU_MODE_PULLUP | SCU_MODE_FUNC5));
		/* Initialize and Configure SSP Peripheral */
		Chip_SSP_Init(LPC_SSP1);
		Chip_SSP_SetMaster(LPC_SSP1, spi.mode);
		switch(spi.clk_mode)
		{
		case MODE0:
			Chip_SSP_SetFormat(LPC_SSP1, SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_CPHA0_CPOL0);
			break;

		case MODE1:
			Chip_SSP_SetFormat(LPC_SSP1, SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_CPHA1_CPOL0);
			break;

		case MODE2:
			Chip_SSP_SetFormat(LPC_SSP1, SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_CPHA0_CPOL1);
			break;

		case MODE3:
			Chip_SSP_SetFormat(LPC_SSP1, SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_CPHA1_CPOL1);
			break;
		}
		Chip_SSP_SetBitRate(LPC_SSP1, spi.bitrate);
		Chip_SSP_Enable(LPC_SSP1);

		ret_value = SUCCESS;
		break;

	default:
		ret_value = ERROR;
		break;
	}

	return ret_value;
}

void SpiRead(spiPort_t port, uint8_t * rx_buffer, uint32_t rx_buffer_size)
{
	Chip_SSP_DATA_SETUP_T data;
	/* Initialize data setup structure */
	data.tx_data = NULL;
	data.tx_cnt = 0;
	data.rx_data = rx_buffer;
	data.rx_cnt = 0;
	data.length = rx_buffer_size;

	switch(port)
		{
		case SPI_1:
			Chip_SSP_RWFrames_Blocking(LPC_SSP1, &data);
			break;
		}
}

void SpiWrite(spiPort_t port, uint8_t * tx_buffer, uint32_t tx_buffer_size)
{
	Chip_SSP_DATA_SETUP_T data;

	/* Initialize data setup structure */
	data.tx_data = tx_buffer;
	data.tx_cnt = 0;
	data.rx_data = NULL;
	data.rx_cnt = 0;
	data.length = tx_buffer_size;

	switch(port)
		{
		case SPI_1:
			Chip_SSP_RWFrames_Blocking(LPC_SSP1, &data);
			break;
		}
}

void SpiReadWrite(spiPort_t port, uint8_t * tx_buffer, uint8_t * rx_buffer, uint32_t buffer_size)
{
	Chip_SSP_DATA_SETUP_T data;

	/* Initialize data setup structure */
	data.tx_data = tx_buffer;
	data.tx_cnt = 0;
	data.rx_data = NULL;
	data.rx_cnt = 0;
	data.length = buffer_size;

	switch(port)
		{
		case SPI_1:
			Chip_SSP_RWFrames_Blocking(LPC_SSP1, &data);
			break;
		}
}
uint8_t SpiDeInit(spiPort_t port)
{
	uint8_t ret_value;

	switch(port)
	{
	case SPI_1:
		Chip_SSP_Disable(LPC_SSP1);
		Chip_SSP_DeInit(LPC_SSP1);
		ret_value = SUCCESS;
		break;

	default:
		ret_value = ERROR;
		break;
	}

	return ret_value;
}
