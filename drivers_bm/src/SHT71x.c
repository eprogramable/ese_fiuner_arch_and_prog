/*
 * SHT71x.c
 *
 *  Created on: Nov 10, 2018
 *      Author: German E. Hachmann, Juan Carlos Neville
 */
#include "sht71x.h"
#include "stopwatch.h"
#include "chip.h"
#include <math.h>


/**
 * @brief Write a Byte
 *
 * Writes a byte on the SHT71x and checks the acknowledge
 *
 * @param [in]: value -> Value to write
 * @return Return a error in case of not acknowledge
 *
 */
uint8_t SHT71xWriteByte(uint8_t value);

/**
 * @brief Read a byte
 *
 * Read a byte from the SHT71x and gives an acknowledge in case of "ack=1"
 *
 * @param [in]: ack
 * @return Byte red
 */
uint8_t SHT71xReadByte(uint8_t ack);

/**
 * @brief Start Transmission
 * Generate the sequence to initiate a transmission.
 */
void SHT71xTxStart(void);

void SetSCK(uint8_t state);
void SetDATA(uint8_t state);


enum {TEMP, HUMI};

#define RESOLUTION8_12Bits		(0 << 0)	/**< (Default) Resolution: 8bits Temp, 12bits RH */
#define RESOLUTION12_14Bits		(1 << 0)	/**< Resolution: 12bits Temp, 14bits RH */
#define RELOADOPT_ON			(0 << 1)
#define RELOADOPT_OFF			(1 << 1)


#define NOACK 	0
#define ACK 	1

#define STATUS_REG_W 	0x06 	/**< Write STATUS Register */
#define STATUS_REG_R 	0x07 	/**< Read STATUS Register */
#define MEASURE_TEMP 	0x03
#define MEASURE_HUMI 	0x05
#define RESET 			0x1e 	/**< Soft Reset */

_pinInfo pinDATAUsed = { /*<GPIO5*/
	.port = 6,
	.pin = 9,
	.gpioPort = 3,
	.gpioPin = 5
};

_pinInfo pinSCKUsed = { /**<GPIO6*/
	.port = 6,
	.pin = 10,
	.gpioPort = 3,
	.gpioPin = 6
};


void SetSCK(uint8_t state){
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, pinSCKUsed.gpioPort, pinSCKUsed.gpioPin, state);
}

void SetDATA(uint8_t state){
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, pinDATAUsed.gpioPort, pinDATAUsed.gpioPin, state);
}


uint8_t SHT71xWriteByte(uint8_t value){
	uint8_t i, error=0;

	for(i=0x80; i>0; i>>=1){
		if(i & value)
			 SetDATA(1);
		else
			 SetDATA(0);
		StopWatch_DelayUs(2);
		SetSCK(1);
		StopWatch_DelayUs(6);
		SetSCK(0);
		StopWatch_DelayUs(2);
	}
	SetDATA(1);
	StopWatch_DelayUs(2);
	SetSCK(1);

	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, pinDATAUsed.gpioPort, pinDATAUsed.gpioPin);
	error = Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, pinDATAUsed.gpioPort, pinDATAUsed.gpioPin);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, pinDATAUsed.gpioPort, pinDATAUsed.gpioPin);

	SetSCK(0);

	return error;
}

uint8_t SHT71xReadByte(uint8_t ack){
	uint8_t i, val=0, data;

	SetDATA(1);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, pinDATAUsed.gpioPort, pinDATAUsed.gpioPin);
	for(i=0x80; i>0; i>>=1){
		SetSCK(1);
		data = Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, pinDATAUsed.gpioPort, pinDATAUsed.gpioPin);
		if(data)
			val |= i;
		SetSCK(1);
	}
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, pinDATAUsed.gpioPort, pinDATAUsed.gpioPin);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, pinDATAUsed.gpioPort, pinDATAUsed.gpioPin, !ack);
	StopWatch_DelayUs(2);
	SetSCK(1);
	StopWatch_DelayUs(6);
	SetSCK(0);
	StopWatch_DelayUs(2);
	SetDATA(1);

	return val;
}

void SHT71xTxStart(void){
	SetDATA(1);
	SetSCK(0);
	StopWatch_DelayUs(2);
	SetSCK(1);
	StopWatch_DelayUs(2);
	SetDATA(0);
	StopWatch_DelayUs(2);
	SetSCK(0);
	StopWatch_DelayUs(6);
	SetSCK(1);
	StopWatch_DelayUs(2);
	SetDATA(1);
	StopWatch_DelayUs(2);
	SetSCK(0);
}

void SHT71xConnectionReset(void){
	uint8_t i;

	SetDATA(1);
	SetSCK(0);
	for(i=0; i<9 ; i++){
		SetSCK(1);
		StopWatch_DelayUs(2);
		SetSCK(0);
	}
	SHT71xTxStart();
}

uint8_t SH71xSoftReset(void){
	uint8_t error=0;

	SHT71xConnectionReset();
	error += SHT71xWriteByte(RESET);

	return error;
}

void SHT71xConfigurePins(_pinInfo apinDATA, _pinInfo apinSCK){
	pinDATAUsed.port = apinDATA.port;
	pinDATAUsed.pin = apinDATA.pin;
	pinDATAUsed.gpioPort = apinDATA.gpioPort;
	pinDATAUsed.gpioPin = apinDATA.gpioPin;

	pinSCKUsed.port = apinSCK.port;
	pinSCKUsed.pin = apinSCK.pin;
	pinSCKUsed.gpioPort = apinSCK.gpioPort;
	pinSCKUsed.gpioPin = apinSCK.gpioPin;
}

void SHT71xInit(){

	Chip_SCU_PinMux(pinDATAUsed.port, pinDATAUsed.pin, MD_PUP, FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, pinDATAUsed.gpioPort, pinDATAUsed.gpioPin);
	Chip_SCU_PinMux(pinSCKUsed.port, pinSCKUsed.pin, MD_PUP, FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, pinSCKUsed.gpioPort, pinSCKUsed.gpioPin);

	StopWatch_Init();

}

uint8_t SHT71xReadStatus(uint8_t *status, uint8_t *checksum){
	uint8_t error = 0;

	SHT71xTxStart();
	error = SHT71xWriteByte(STATUS_REG_R);
	*status = SHT71xReadByte(ACK);
	*checksum = SHT71xReadByte(NOACK);

	return error;
}

uint8_t SHT71xWriteStatus(uint8_t value){
	uint8_t error = 0;

	SHT71xTxStart();
	error += SHT71xWriteByte(STATUS_REG_W);
	error += SHT71xWriteByte(value);

	return error;
}

uint8_t SHT71xMeasureTemp(uint16_t *tempValue, uint8_t *checksum, uint8_t mode){
	uint8_t error = 0;
	uint16_t i;
	uint8_t data;

	SHT71xTxStart();
	error +=SHT71xWriteByte(MEASURE_TEMP);

	if(!mode){
		Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, pinDATAUsed.gpioPort, pinDATAUsed.gpioPin);
		i = 1000;
		data = 1;
		while(data && i){
			StopWatch_DelayMs(1);
			data = Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, pinDATAUsed.gpioPort, pinDATAUsed.gpioPin);
			i--;
		}
		Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, pinDATAUsed.gpioPort, pinDATAUsed.gpioPin);
		if(data)
			error++;
		*((uint8_t *)(tempValue)) = SHT71xReadByte(ACK);
		*((uint8_t *)(tempValue)+1) = SHT71xReadByte(ACK);
		*checksum = SHT71xReadByte(ACK);
	}
	else{
		*tempValue = 0xFFFF;
		*checksum = 0xFF;
	}

	return error;
}

uint8_t SHT71xMeasureRH(uint16_t *rhValue, uint8_t *checksum, uint8_t mode){
	uint8_t error = 0;
	uint16_t i;
	uint8_t data;

	SHT71xTxStart();
	error +=SHT71xWriteByte(MEASURE_HUMI);

	if(!mode){
		Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, pinDATAUsed.gpioPort, pinDATAUsed.gpioPin);
		i = 1000;
		data = 1;
		while(data && i){
			StopWatch_DelayMs(1);
			data = Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, pinDATAUsed.gpioPort, pinDATAUsed.gpioPin);
			i--;
		}
		Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, pinDATAUsed.gpioPort, pinDATAUsed.gpioPin);
		if(data)
			error++;
		*((uint8_t *)(rhValue)) = SHT71xReadByte(ACK);
		*((uint8_t *)(rhValue)+1) = SHT71xReadByte(ACK);
		*checksum = SHT71xReadByte(ACK);
	}
	else{
		*rhValue = 0xFFFF;
		*checksum = 0xFF;
	}

	return error;
}

float SHT71xCalcCentigrade(uint16_t tempValue){
	float temp; //14 Bits

	temp = tempValue*0.01 - 40.1;

	return temp;
}

float SHT71xCalcFarenheight(uint16_t tempValue){
	float tempFarengeith = 0;

	return tempFarengeith;
}

float SHT71xCalcRH(uint16_t rhValue, float tempValue){
	float C1 = -2.0468; 		// for 12 Bit RH
	float C2 = 0.0367; 			// for 12 Bit RH
	float C3 = -0.0000015955; 	// for 12 Bit RH
	float T1 = 0.01; 			// for 12 Bit RH
	float T2 = 0.00008; 		// for 12 Bit RH
	float rh_lin; 				// rh_lin: Humidity linear
	float rh_true; 				// rh_true: Temperature compensated humidity

	rh_lin = C3*rhValue*rhValue + C2*rhValue + C1;
	rh_true = (tempValue-25)*(T1+T2*rhValue) + rh_lin;
	if(rh_true>100)
		rh_true = 100;
	if(rh_true<0.1)
		rh_true = 0.1;

	return rh_true;
}

float SHT71xCalcDewPoint(float rhValue, float tempValue){
	float k, dew_point;

	k = (log10(rhValue)-2)/0.4343 + (17.62*tempValue)/(243.12+tempValue);

	dew_point = 243.12*k/(17.62-k);

	return dew_point;
}


