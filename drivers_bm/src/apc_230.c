/*
 * apc_230.c
 *
 *  Created on: 23 nov. 2018
 *      Author: pola
 */

/*==================[inclusions]=============================================*/
#include "chip.h"
#include "apc_230.h"

/*==================[macros and definitions]=================================*/
#define GPIO_PORT_EN	0x03
#define GPIO_PIN_EN 	0x03
#define GPIO_PORT_SET 	0x03
#define GPIO_PIN_SET 	0x04

const s_parity uart_parity[] = {
		{'0', UART_LCR_PARITY_DIS},
		{'1', UART_LCR_PARITY_ODD},
		{'2', UART_LCR_PARITY_EVEN}
};

const s_rate usart_baud_rate[] = {
		{'0', 1200},
		{'1', 2400},
		{'2', 4800},
		{'3', 9600},
		{'4', 19200},
		{'5', 38400},
		{'6', 57600}
};


/*==================[internal data declaration]==============================*/
pFunc FuncRx;
void(*pfunca)(void);
apc_conf apc_cf;
static uint8_t flag=0;
char *data;

/*==================[internal functions declaration]=========================*/
/** \brief Configuration function of UART3 P2_3(RS232 TXD) P2_4(RS232 RXD) and Pins Module (GPIO1 y GPIO2)*/
uint8_t ApcInit(void)
{
	  Chip_SCU_PinMuxSet(0x02, 0x03, SCU_MODE_PULLDOWN | SCU_MODE_FUNC2);
	  Chip_SCU_PinMuxSet(0x02, 0x04, SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS | SCU_MODE_FUNC2);
	  Chip_UART_Init(LPC_USART3);
	  Chip_UART_ConfigData(LPC_USART3, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT | UART_LCR_PARITY_DIS);
	  Chip_UART_SetBaud(LPC_USART3, 9600);
	  Chip_UART_SetupFIFOS(LPC_USART3, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |	UART_FCR_TX_RS | UART_FCR_TRG_LEV3));
	  Chip_UART_IntEnable(LPC_USART3, UART_IER_RBRINT);

	  Chip_SCU_PinMuxSet(0x06, 0x04, SCU_MODE_INACT | SCU_MODE_FUNC0);	/// ENABLE 1 -> Para la configuración como para la transmisión y recepción de datos
	  Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_EN, GPIO_PIN_EN);
//	  Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, GPIO_PORT_EN, GPIO_PIN_EN);

	  Chip_SCU_PinMuxSet(0x06, 0x05, SCU_MODE_INACT | SCU_MODE_FUNC0);	///    SET  1 -> Estado de funcionamiento normal (RUNNING)
	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  		/// 		0 -> Estado de configuración (SETTING)
	  Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, GPIO_PORT_SET, GPIO_PIN_SET);
//	  Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, GPIO_PORT_SET, GPIO_PIN_SET);
	  ApcConfMode();
}


uint8_t ApcConfig(apc_conf *conf)
{
	memcpy(&apc_cf, conf, sizeof(apc_conf));
//	Chip_UART_ConfigData(LPC_USART3, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT | uart_parity[conf.parity].uart_odd);
//	Chip_UART_SetBaud(LPC_USART3, usart_baud_rate[conf.serie_rate].uart_bau);

//	Chip_UART_ConfigData(LPC_USART3, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT | uart_parity[0].uart_odd);
//	Chip_UART_SetBaud(LPC_USART3, usart_baud_rate[3].uart_bau);

//	Chip_UART_ConfigData(LPC_USART3, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT | UART_LCR_PARITY_DIS);
//	Chip_UART_SetBaud(LPC_USART3, 9600);
}

void ApcTx(char dato)
{}
void ApcTxBuff(const char * buff)
{
	Chip_UART_SendBlocking(CHIP_USART, (const void*) buff, strlen(buff));
}
char ApcRx(void)
{}
void ApcRxBuff(char * buff, int size)
{
	Chip_UART_ReadBlocking(LPC_USART3, buff, size);
}
void ApcOn(void)
{
	ApcInit();
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, GPIO_PORT_EN, GPIO_PIN_EN);
	ApcCheck();
}
uint8_t ApcIsOn(void)
{
	return flag;
}
void ApcOff(void)
{
	Chip_GPIO_SetPinOutOff(LPC_GPIO_PORT, GPIO_PORT_EN, GPIO_PIN_EN);
	flag=0;
}

void ApcCheck(void)
{
	flag=0;
	Chip_UART_SendBlocking(LPC_USART2,"WR 434000 3 9 0 0\r\n", 19);
}

void UART2_IRQHandler(void)
{
	char c;
	if(flag==0)
	{
		Chip_UART_ReadBlocking(LPC_USART2, &c, 1);
		if(c=='A')
			flag=1;
		else
			ApcCheck();
	}else
	{
		apc_cf.Rx();

	}

}

void ApcConfMode(void)
{
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, GPIO_PORT_SET, GPIO_PIN_SET);
}
void ApcRunMode(void)
{
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT, GPIO_PORT_SET, GPIO_PIN_SET);
}
