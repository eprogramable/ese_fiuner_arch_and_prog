/* @brief  EDU-CIAA NXP PN532 driver
 * @author Juan Ignacio Cerrudo
 *
 * This driver provide functions to configure and handle a PN532 NFC module
 *
 * @note
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 23/11/2018 | Document creation		                         |
 *
 */
#include "spi.h"
#include "gpio.h"
#include "pn532.h"
#include "sapi.h"
#include "string.h"


#define SUCCESS	1			/* */
#define ERROR 	0			/* */
#define BITS8	8

DEBUG_PRINT_ENABLE;
//#define PN532_P2P_DEBUG
//#define PN532DEBUG

uint8_t pn532ack[] = { 0x00, 0x00, 0xFF, 0x00, 0xFF, 0x00};
uint8_t pn532response_firmwarevers[] = {0x00, 0xFF, 0x06, 0xFA, 0xD5, 0x03};

#define PN532_PACK_BUFF_SIZE 64
uint8_t pn532_packetbuffer[PN532_PACK_BUFF_SIZE];

spiConfig_t spi_conf= {SPI_1, MASTER, MODE0, MSB_FIRST, 100000};
gpioPin_t spi_chip_select;

/*****************************************************************************
 * Private functions definitions
 ****************************************************************************/

/**
 * @brief		Reverse the bits of each element of the buffer
 * @param[out]	rev_data Pointer to buffer with data to be reversed
 * @param[in]	data_length Buffer size
 * @return		Nothing
 * @note
 */
void ReverseBits(uint8_t * rev_data, uint8_t data_length);

/*****************************************************************************
 * Private functions declarations
 ****************************************************************************/

void ReverseBits(uint8_t * rev_data, uint8_t data_length)
{
	uint8_t data;

	for (uint8_t i = 0; i < data_length; i++)
	{
		data = 0;
		for (uint8_t j = 0; j < BITS8; j++)
		{
			if (rev_data[i] & (1 << j))
			{
				data |= (1 << ((BITS8 - 1) - j));
			}
		}
		rev_data[i] = data;
	}
}


/*****************************************************************************
 * Public functions declarations
 ****************************************************************************/
void PN532Init(uint8_t port, uint32_t spi_br, uint8_t spi_cs)
{
	/*The mode of the SPI interface should be set at mode0
	   	  according to the datasheet of PN532, lsb first */
	spi_conf.port = port;
	spi_conf.bitrate = spi_br;
	spi_chip_select = spi_cs;
	gpioConf_t gpio_port ={spi_cs, OUTPUT, NONE_RES};
	GPIOInit(gpio_port);

	GPIOSetLow(spi_chip_select);
    delay(1000);
    /*Not exactly sure why but we have to send a dummy command to get synced up*/
    pn532_packetbuffer[0] = PN532_FIRMWAREVERSION;
    /*Ignore response!*/
    PN532SendCommandCheckAck(pn532_packetbuffer, 1, 1000);
}

uint32_t PN532GetFirmwareVersion(void)
{
  uint32_t response;
  uint8_t n=0;
  pn532_packetbuffer[0] = PN532_FIRMWAREVERSION;

  if (!PN532SendCommandCheckAck(pn532_packetbuffer, 1, 1000))
    return 0;

  //Read data packet
  PN532ReadBytes(pn532_packetbuffer, 12);
  //Check some basic stuff

  while(n<5){
  	  n++;
  	  if (pn532_packetbuffer[n] != pn532response_firmwarevers[n]){
  		  return *(uint8_t*)(pn532_packetbuffer-1) - *(uint8_t*)(pn532response_firmwarevers-1);
  	  }
    }
    //return TRUE;

  /*if (0 != strncmp((char *)pn532_packetbuffer, (char *)pn532response_firmwarevers, 6))
  {
    return 0;
  }
 */
  response = pn532_packetbuffer[6];
  response <<= 8;
  response |= pn532_packetbuffer[7];
  response <<= 8;
  response |= pn532_packetbuffer[8];
  response <<= 8;
  response |= pn532_packetbuffer[9];

  return response;
}



bool_t PN532SendCommandCheckAck(uint8_t* cmd, uint8_t cmd_len, uint16_t timeout)
{
  uint16_t timer = 0;
  // PN532Write the command
  PN532WriteCommand(cmd, cmd_len);

  // Wait for chip to say it's ready!
  while (PN532ReadSpiStatus() != PN532_SPI_READY) {
    if (timeout != 0) {
      timer+=10;
      if (timer > timeout)
        return ERROR;
    }
    delay(10);
  }

  // read acknowledgement
  if (!PN532CheckSpiAck()) {
    return ERROR;
  }

  timer = 0;
  // Wait for chip to say its ready!
  while (PN532ReadSpiStatus() != PN532_SPI_READY) {
    if (timeout != 0) {
      timer+=10;
      if (timer > timeout)
        return ERROR;
    }
    delay(10);
  }

  return SUCCESS; // ack'd command
}


bool_t PN532SAMConfig(void)
{
  pn532_packetbuffer[0] = PN532_SAMCONFIGURATION;
  pn532_packetbuffer[1] = 0x01; // normal mode;
  pn532_packetbuffer[2] = 0x14; // timeout 50ms * 20 = 1 second
  pn532_packetbuffer[3] = 0x01; // use IRQ pin!

  if (! PN532SendCommandCheckAck(pn532_packetbuffer, 4, 1000))
    return ERROR;

  // read data packet
  PN532ReadBytes(pn532_packetbuffer, 8);

  return  (pn532_packetbuffer[5] == 0x15);
}


uint32_t PN532ConfigurePeerAsInitiator(uint8_t baudrate)
{

  pn532_packetbuffer[0] = PN532_INJUMPFORDEP;
  pn532_packetbuffer[1] = 0x01; //Active Mode
  pn532_packetbuffer[2] = baudrate;// Use 1 or 2. //0 i.e 106kps is not supported yet
  pn532_packetbuffer[3] = 0x01; //Indicates Optional Payload is present

  //Polling request payload
  pn532_packetbuffer[4] = 0x00;
  pn532_packetbuffer[5] = 0xFF;
  pn532_packetbuffer[6] = 0xFF;
  pn532_packetbuffer[7] = 0x00;
  pn532_packetbuffer[8] = 0x00;

  if (!PN532SendCommandCheckAck(pn532_packetbuffer, 9, 1000))
    return ERROR;

  // read data packet
  PN532ReadBytes(pn532_packetbuffer, 19+6);

#ifdef PN532DEBUG
  debugPrintEnter();
  // check the response
  debugPrintString("PEER_INITIATOR");
  for(uint8_t i=0;i<19+6;i++)
  {
	  debugPrintIntFormat(pn532_packetbuffer[i], HEX_FORMAT);
	  debugPrintString(" ");
  }
#endif

  return (pn532_packetbuffer[7] == 0x00); //No error

}

bool_t PN532InitiatorTxRx(char* dataOut,char* dataIn)
{
  pn532_packetbuffer[0] = PN532_INDATAEXCHANGE;
  pn532_packetbuffer[1] = 0x01; //Target 01

    for(uint8_t iter=(2+0);iter<(2+16);iter++)
  {
    pn532_packetbuffer[iter] = dataOut[iter-2]; //pack the data to send to target
  }

  if (! PN532SendCommandCheckAck(pn532_packetbuffer, 18, 1000))
    return ERROR;

  // read data packet
  PN532ReadBytes(pn532_packetbuffer, 18+6);

#ifdef PN532_P2P_DEBUG
  debugPrintEnter();
  // check the response
  debugPrintString("INITIATOR receive:");
  debugPrintEnter();
  for(uint8_t i=0;i<18+6;i++)
  {
	  debugPrintIntFormat(pn532_packetbuffer[i], HEX_FORMAT);
	  debugPrintString(" ");
  }
#endif

  for(uint8_t iter=8;iter<(8+16);iter++)
  {
    dataIn[iter-8] = pn532_packetbuffer[iter]; //data received from target
  }

  return (pn532_packetbuffer[7] == 0x00); //No error
}

uint32_t PN532ConfigurePeerAsTarget()
{
  uint8_t pbuffer[38] =      {
    PN532_TGINITASTARGET,
    0x00,
    0x08, 0x00, //SENS_RES
    0x12, 0x34, 0x56, //NFCID1
    0x40, //SEL_RES
    0x01, 0xFE, 0xA2, 0xA3, 0xA4, 0xA5, 0xA6, 0xA7, // POL_RES
    0xC0, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7,
    0xFF, 0xFF,
    0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, //NFCID3t: Change this to desired value
    0x00, 0x00 //Length of general and historical uint8_ts
  };
  for(uint8_t i = 0;i < 38;i ++)
  {
    pn532_packetbuffer[i] = pbuffer[i];
  }
  if (! PN532SendCommandCheckAck(pn532_packetbuffer, 38, 1000))
    return ERROR;

  // read data packet
  PN532ReadBytes(pn532_packetbuffer, 18+6);

#ifdef PN532DEBUG
  debugPrintEnter();
  // check some basic stuff

  debugPrintString("PEER_TARGET");
  for(uint8_t i=0;i<18+6;i++)
  {
	  debugPrintIntFormat(pn532_packetbuffer[i], HEX_FORMAT);
	  debugPrintString(" ");
  }
#endif

  return (pn532_packetbuffer[23] == 0x00); //No error as it received all response
}
/*Function: Recieve data first and then transmit data to the initiator*/
uint32_t PN532TargetTxRx(char* dataOut,char* dataIn)
{
  /* Receiving from Initiator */
  pn532_packetbuffer[0] = PN532_TGGETDATA;
  if (! PN532SendCommandCheckAck(pn532_packetbuffer, 1, 1000))
    return ERROR;

  // read data packet
  PN532ReadBytes(pn532_packetbuffer, 18+6);

#ifdef PN532_P2P_DEBUG
  debugPrintEnter();
  // check the response
  debugPrintString("TARGET RX:");
  for(uint8_t i=0;i<18+6;i++)
  {
	  debugPrintIntFormat(pn532_packetbuffer[i], HEX_FORMAT);
	  debugPrintString(" ");
  }
#endif

  for(uint8_t iter=8;iter<(8+16);iter++)
  {
    dataIn[iter-8] = pn532_packetbuffer[iter]; //data received from initiator
  }

  /* Sending to Initiator */
  if(pn532_packetbuffer[7] == 0x00) //If no errors in receiving, send data.
  {
    pn532_packetbuffer[0] = PN532_TGSETDATA;
    for(uint8_t iter=(1+0);iter<(1+16);iter++)
    {
      pn532_packetbuffer[iter] = dataOut[iter-1]; //pack the data to send to target
    }

    if (! PN532SendCommandCheckAck(pn532_packetbuffer, 17, 1000))
      return ERROR;

    // read data packet
    PN532ReadBytes(pn532_packetbuffer, 2+6);

#ifdef PN532_P2P_DEBUG
    debugPrintEnter();
    // check the response
    debugPrintString("TARGET get response after transmiting: ");
    for(uint8_t i=0;i<2+6;i++)
    {
    	debugPrintIntFormat(pn532_packetbuffer[i], HEX_FORMAT);
    	debugPrintString(" ");
    }
#endif


  }
  return (pn532_packetbuffer[7] == 0x00); //No error
}

uint32_t PN532AuthenticateBlock(uint8_t cardnumber /*1 or 2*/,uint32_t cid /*Card NUID*/, uint8_t blockaddress /*0 to 63*/,uint8_t authtype/*Either KEY_A or KEY_B */, uint8_t * keys)
{
  pn532_packetbuffer[0] = PN532_INDATAEXCHANGE;
  pn532_packetbuffer[1] = cardnumber;  // either card 1 or 2 (tested for card 1)
  if(authtype == KEY_A)
  {
    pn532_packetbuffer[2] = PN532_AUTH_WITH_KEYA;
  }
  else
  {
    pn532_packetbuffer[2] = PN532_AUTH_WITH_KEYB;
  }
  pn532_packetbuffer[3] = blockaddress; //This address can be 0-63 for MIFARE 1K card

  pn532_packetbuffer[4] = keys[0];
  pn532_packetbuffer[5] = keys[1];
  pn532_packetbuffer[6] = keys[2];
  pn532_packetbuffer[7] = keys[3];
  pn532_packetbuffer[8] = keys[4];
  pn532_packetbuffer[9] = keys[5];

  pn532_packetbuffer[10] = ((cid >> 24) & 0xFF);
  pn532_packetbuffer[11] = ((cid >> 16) & 0xFF);
  pn532_packetbuffer[12] = ((cid >> 8) & 0xFF);
  pn532_packetbuffer[13] = ((cid >> 0) & 0xFF);

  if (! PN532SendCommandCheckAck(pn532_packetbuffer, 14, 1000))
    return ERROR;

  // read data packet
  PN532ReadBytes(pn532_packetbuffer, 2+6);

#ifdef PN532DEBUG
  for(int iter=0;iter<14;iter++)
  {
	  debugPrintHex(pn532_packetbuffer[iter], HEX_FORMAT);
	  debugPrintString(" ");
  }
  debugPrintEnter();
  // check some basic stuff

  debugPrintString("AUTH");
  for(uint8_t i=0;i<2+6;i++)
  {
	  debugPrintIntFormat(pn532_packetbuffer[i], HEX_FORMAT);
	  debugPrintString(" ");
  }
#endif

  if((pn532_packetbuffer[6] == 0x41) && (pn532_packetbuffer[7] == 0x00))
  {
    return SUCCESS;
  }
  else
  {
    return ERROR;
  }

}

bool_t PN532ReadMemoryBlock(uint8_t cardnumber,uint8_t blockaddress,uint8_t * block)
{
  pn532_packetbuffer[0] = PN532_INDATAEXCHANGE;
  pn532_packetbuffer[1] = cardnumber;  // either card 1 or 2 (tested for card 1)
  pn532_packetbuffer[2] = PN532_MIFARE_READ;
  pn532_packetbuffer[3] = blockaddress; //This address can be 0-63 for MIFARE 1K card

  if (! PN532SendCommandCheckAck(pn532_packetbuffer, 4, 1000))
    return ERROR;

  // read data packet
  PN532ReadBytes(pn532_packetbuffer, 18+6);
  // check some basic stuff
#ifdef PN532DEBUG
  debugPrintString("READ");
#endif
  for(uint8_t i=8;i<18+6;i++)
  {
    block[i-8] = pn532_packetbuffer[i];
#ifdef PN532DEBUG
    debugPrintIntFormat(pn532_packetbuffer[i], HEX_FORMAT);
    debugPrintString(" ");
#endif
  }
  if((pn532_packetbuffer[6] == 0x41) && (pn532_packetbuffer[7] == 0x00))
  {
    return SUCCESS; //read successful
  }
  else

  {
    return ERROR;
  }

}



bool_t PN532WriteMemoryBlock(uint8_t cardnumber,uint8_t blockaddress,uint8_t * block)
{
  pn532_packetbuffer[0] = PN532_INDATAEXCHANGE;
  pn532_packetbuffer[1] = cardnumber;  // either card 1 or 2 (tested for card 1)
  pn532_packetbuffer[2] = PN532_MIFARE_WRITE;
  pn532_packetbuffer[3] = blockaddress;

  for(uint8_t uint8_t=0; uint8_t <16; uint8_t++)
  {
    pn532_packetbuffer[4+uint8_t] = block[uint8_t];
  }

  if (! PN532SendCommandCheckAck(pn532_packetbuffer, 20, 1000))
    return ERROR;
  // read data packet
  PN532ReadBytes(pn532_packetbuffer, 2+6);

#ifdef PN532DEBUG
  // check some basic stuff
  debugPrintString("PN532Write: ");
  for(uint8_t i=0;i<2+6;i++)
  {
	  debugPrintIntFormat(pn532_packetbuffer[i], HEX_FORMAT);
	  debugPrintString(" ");
  }
#endif

  if((pn532_packetbuffer[6] == 0x41) && (pn532_packetbuffer[7] == 0x00))
  {
    return SUCCESS; //PN532Write successful
  }
  else
  {
    return ERROR;
  }
}

uint32_t PN532ReadPassiveTargetID(uint8_t cardbaudrate)
{
  uint32_t cid;

  pn532_packetbuffer[0] = PN532_INLISTPASSIVETARGET;
  pn532_packetbuffer[1] = 1;  // max 1 cards at once (we can set this to 2 later)
  pn532_packetbuffer[2] = cardbaudrate;

  if (! PN532SendCommandCheckAck(pn532_packetbuffer, 3, 1000))
    return 0x0;  // no cards read

  // read data packet
  PN532ReadBytes(pn532_packetbuffer, 20);
  // check some basic stuff
#ifdef PN532DEBUG
  debugPrintString("Found ");
  debugPrintIntFormat(pn532_packetbuffer[7], DEC_FORMAT);
  debugPrintString(" tags");
  if (pn532_packetbuffer[7] != 1)
    return 0;
  debugPrintString("\r\n");
#endif
  uint16_t sens_res = pn532_packetbuffer[9];
  sens_res <<= 8;
  sens_res |= pn532_packetbuffer[10];
#ifdef PN532DEBUG
  debugPrintString("Sens Response: 0x");
  debugPrintIntFormat(sens_res, HEX_FORMAT);
  debugPrintString("\r\n");
  debugPrintString("Sel Response: 0x");
  debugPrintIntFormat(pn532_packetbuffer[11], HEX_FORMAT);
#endif
  cid = 0;
  for (uint8_t i=0; i< pn532_packetbuffer[12]; i++) {
    cid <<= 8;
    cid |= pn532_packetbuffer[13+i];
#ifdef PN532DEBUG
    debugPrintString(" 0x");
    debugPrintIntFormat(pn532_packetbuffer[13+i], HEX_FORMAT);
#endif
  }
#ifdef PN532DEBUG
  debugPrintString("\r\n");
  debugPrintString("TargetID");
  for(uint8_t i=0;i<20;i++)
  {
	  debugPrintHex(pn532_packetbuffer[i], HEX_FORMAT);
	  debugPrintString(" ");
  }
#endif
  return cid;
}

/************** low level SPI ********/
/*Function:Transmit a uint8_t to PN532 through the SPI interface. */
inline void PN532Write(uint8_t data)
{
	SpiInit(spi_conf);
	ReverseBits(&data, 1);
	SpiWrite(spi_conf.port, &data, 1);
}

/*Function:Receive a uint8_t from PN532 through the SPI interface */
inline uint8_t PN532Read(void)
{
	uint8_t data;
	SpiInit(spi_conf);
	SpiRead(spi_conf.port, &data, 1);
	ReverseBits(&data, 1);
    return data;
}


/************** mid level SPI */

uint8_t PN532ReadSpiStatus(void)
{
  GPIOSetLow(spi_chip_select);
  delay(2);
  PN532Write(PN532_SPI_STATREAD);
  uint8_t status = PN532Read();
  GPIOSetHigh(spi_chip_select);
  return status;
}

/************** high level SPI */
bool_t PN532CheckSpiAck()
{
  uint8_t ackbuff[6];
  uint8_t n = 0;
  PN532ReadBytes(ackbuff, 6);
  while(n<5){
	  n++;
	  if (ackbuff[n] != pn532ack[n]){
		  return *(uint8_t*)(ackbuff-1) - *(uint8_t*)(pn532ack-1);
	  }
  }
  return TRUE;


  //return (0 == strncmp((char *)ackbuff, (char *)pn532ack, 6));
}

void PN532ReadBytes(uint8_t* buff, uint8_t n)
{
  GPIOSetLow(spi_chip_select);
  delay(2);
  PN532Write(PN532_SPI_DATAREAD);

#ifdef PN532DEBUG
  debugPrintString("Reading: ");
#endif
  for (uint8_t i=0; i < n; i ++)
  {
    delay(1);
    buff[i] = PN532Read();
#ifdef PN532DEBUG
    debugPrintString(" 0x");
    debugPrintIntFormat(buff[i], HEX_FORMAT);
#endif
  }

#ifdef PN532DEBUG
  debugPrintEnter();
#endif

  GPIOSetHigh(spi_chip_select);
}

void PN532WriteCommand(uint8_t* cmd, uint8_t cmd_len)
{
  uint8_t checksum;

  cmd_len++;

#ifdef PN532DEBUG
  debugPrintString("\nSending: ");
#endif

  GPIOSetLow(spi_chip_select);
  delay(2);     // or whatever the delay is for waking up the board
  PN532Write(PN532_SPI_DATAWRITE);

  checksum = PN532_PREAMBLE + PN532_PREAMBLE + PN532_STARTCODE2;
  PN532Write(PN532_PREAMBLE);
  PN532Write(PN532_PREAMBLE);
  PN532Write(PN532_STARTCODE2);

  PN532Write(cmd_len);
  uint8_t cmdlen_1=~cmd_len + 1;
  PN532Write(cmdlen_1);

  PN532Write(PN532_HOSTTOPN532);
  checksum += PN532_HOSTTOPN532;

#ifdef PN532DEBUG
  debugPrintString(" 0x");
  debugPrintIntFormat(PN532_PREAMBLE, HEX_FORMAT);
  debugPrintString(" 0x");
  debugPrintIntFormat(PN532_PREAMBLE, HEX_FORMAT);
  debugPrintString(" 0x");
  debugPrintIntFormat(PN532_STARTCODE2, HEX_FORMAT);
  debugPrintString(" 0x");
  debugPrintIntFormat(cmd_len, HEX_FORMAT);
  debugPrintString(" 0x");
  debugPrintIntFormat(cmdlen_1, HEX_FORMAT);
  debugPrintString(" 0x");
  debugPrintIntFormat(PN532_HOSTTOPN532, HEX_FORMAT);
#endif

  for (uint8_t i=0; i<cmd_len-1; i++)
  {
    PN532Write(cmd[i]);
    checksum += cmd[i];
#ifdef PN532DEBUG
    debugPrintString(" 0x");
    debugPrintIntFormat(cmd[i], HEX_FORMAT);
#endif
  }
  uint8_t checksum_1=~checksum;
  PN532Write(checksum_1);
  PN532Write(PN532_POSTAMBLE);
  GPIOSetHigh(spi_chip_select);

#ifdef PN532DEBUG
  debugPrintString(" 0x");
  debugPrintIntFormat(checksum_1, HEX_FORMAT);
  debugPrintString(" 0x");
  debugPrintIntFormat(PN532_POSTAMBLE, HEX_FORMAT);
  debugPrintEnter();
#endif
}
