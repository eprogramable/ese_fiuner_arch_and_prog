/* # Driver for gy271 magnetometer, a HMC5883L breakout #
 *  \brief
 *  \file: gy271_magnetometer.h
 *  \date 16 nov. 2018
 *  \author: Esteban Osella
 */

/**
* Register List
*/

#include "gy271_magnetometer.h"
#include "gy271_HWInterface.h"


//uint8_t gy271_i2c_inited = 0;


enum{
   /* EDU-CIAA-NXP */

   // P1 header
   T_FIL1,    T_COL2,    T_COL0,    T_FIL2,      T_FIL3,  T_FIL0,     T_COL1,
   CAN_TD,    CAN_RD,    RS232_TXD, RS232_RXD,

   // P2 header
   GPIO8,     GPIO7,     GPIO5,     GPIO3,       GPIO1,
   LCD1,      LCD2,      LCD3,      LCDRS,       LCD4,
   SPI_MISO,
   ENET_TXD1, ENET_TXD0, ENET_MDIO, ENET_CRS_DV, ENET_MDC, ENET_TXEN, ENET_RXD1,
   GPIO6,     GPIO4,     GPIO2,     GPIO0,
   LCDEN,
   SPI_MOSI,
   ENET_RXD0,

   // Switches
   // 36   37     38     39
   TEC1,  TEC2,  TEC3,  TEC4,

   // Leds
   // 40   41     42     43     44     45
   LEDR,  LEDG,  LEDB,  LED1,  LED2,  LED3,

   /* CIAA-NXP */
 /* 46     47     48     49     50     51     52     53 */
   DI0,   DI1,   DI2,   DI3,   DI4,   DI5,   DI6,   DI7,
 /* 54     55     56     57     58     59     60     61 */
   DO0,   DO1,   DO2,   DO3,   DO4,   DO5,   DO6,   DO7
};




uint8_t GY271ConfigDevice(GY271_magnetometer_t GY271_dev)
{
	return gy271HWIConfigDevice(GY271_dev);
}

uint16_t GY271ConfigAll()
{
	uint16_t config_ok = 0;

	/*if(!gy271_i2c_inited)
	{
		I2CInit();
		gy271_i2c_inited = 1;
	}*/

	uint8_t max_rate = GY271_0_75HZ;

	uint16_t iter = 0;

	for(iter = 0; iter < GY271_MAX_ITEMS ; iter ++)
	{
		if(GY271_devices[iter].config.rate > max_rate)
		{
			max_rate = GY271_devices[iter].config.rate ;
		}
	}

	for(iter = 0; iter < GY271_MAX_ITEMS ; iter ++)
	{
		GY271_devices[iter].config.rate = max_rate;
		switch(iter)
		{
		case 0:
			GY271_devices[iter].config.GPIO_DRDY = GPIO0;
			break;
		case 1:
			GY271_devices[iter].config.GPIO_DRDY = GPIO1;
			break;
		case 2:
			GY271_devices[iter].config.GPIO_DRDY = GPIO2;
			break;
		case 3:
			GY271_devices[iter].config.GPIO_DRDY = GPIO3;
			break;
		}
		if (GY271ConfigDevice(GY271_devices[iter]))
		{
			config_ok++;
		}
	}
    return config_ok;
}



uint8_t GY271ReadDevice(GY271_magnetometer_t * GY271_dev)
{
	/*uint8_t read_res, out,reg_add;

	uint8_t x_MSB, x_LSB;
	uint8_t y_MSB, y_LSB;
	uint8_t z_MSB, z_LSB;

	reg_add = GY271_DOUT_X_MSB_REG;

	out = I2CRead(GY271_dev->base_address, &reg_add , sizeof(reg_add), &x_MSB, sizeof(x_MSB));
	reg_add = GY271_DOUT_X_LSB_REG;
	read_res = I2CRead(GY271_dev->base_address, &reg_add , sizeof(reg_add ), &x_LSB, sizeof(x_LSB));
	GY271_dev->x_read = (x_MSB << 8 ) | x_LSB;
	out = (out << 1 ) | read_res;

	reg_add = GY271_DOUT_Z_MSB_REG;
	read_res = I2CRead(GY271_dev->base_address, &reg_add , sizeof(reg_add), &z_MSB, sizeof(z_MSB));
	out = (out << 1 ) | read_res;
	reg_add = GY271_DOUT_Z_LSB_REG;
	read_res = I2CRead(GY271_dev->base_address, &reg_add , sizeof(reg_add ), &z_LSB, sizeof(z_LSB));
	out = (out << 1 ) | read_res;
	GY271_dev->z_read = (z_MSB << 8) | z_LSB;

	reg_add = GY271_DOUT_Y_MSB_REG;
	read_res = I2CRead(GY271_dev->base_address, &reg_add , sizeof(reg_add), &y_MSB, sizeof(y_MSB));
	out = (out << 1 ) | read_res;
	reg_add = GY271_DOUT_Y_LSB_REG;
	read_res = I2CRead(GY271_dev->base_address, &reg_add , sizeof(reg_add ), &y_LSB, sizeof(y_LSB));
	out = (out << 1 ) | read_res;
	GY271_dev->y_read = (y_MSB << 8) | y_LSB;*/

	return GY271HWIReadDevice(GY271_dev);
}

uint16_t GY271ReadAll()
{
	uint16_t iter,read_ok = 0;
	for(iter = 0; iter < GY271_MAX_ITEMS ; iter ++)
	{
		if (GY271ReadDevice(&GY271_devices[iter]))
		{
			read_ok++;
		}
	}
	return read_ok;
}

void GY271GetDefaultConfig(GY271_config_t * config)
{
	config->flow = GY271_NORM_MEASUREMENT;
	config->gain = GY271_GAIN_DEF;
	config->mode = GY271_DEFAULT_MODE;
	config->rate = GY271_DEF_HZ;
	config->samples = GY271_D_SAMPLE;
	config->GPIO_DRDY = GPIO0;
}


void GetGaussReadings(GY271_magnetometer_t GY271_dev, float * x_read_guass, float * y_read_guass, float * z_read_guass)
{
	uint16_t gain = 0;
	switch (GY271_dev.config.gain)
	{
		case GY271_GAIN_088:
			gain = 1370;
			break;
		case GY271_GAIN_130:
				gain = 1090;
				break;
		case GY271_GAIN_190:
				gain = 820;
				break;
		case GY271_GAIN_250:
				gain = 660;
				break;
		case GY271_GAIN_400:
				gain = 440;
				break;
		case GY271_GAIN_470:
				gain = 390;
				break;
		case GY271_GAIN_560:
				gain = 330;
				break;
		case GY271_GAIN_810:
				gain = 230;
				break;
		default:
			gain = 1090;
	}
	float a = (float )GY271_dev.x_read;
	* x_read_guass = a / gain;
	a = (float) GY271_dev.y_read;
	* y_read_guass = (a) / gain;
	a = (float) GY271_dev.z_read;
	* z_read_guass = (a) / gain;

}

