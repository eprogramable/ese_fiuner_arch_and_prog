/* Copyright 2018,
 * Sebastian Mateos
 * smateos@ingenieria.uner.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "spi.h"
#include "chip.h"
#include <stdint.h>
#include "bool.h"

/** @addtogroup spi
 *  @{
 */

#define SPI_CLK0_PORT  15
#define SPI_CLK0_PIN   4
#define SPI_MISO1_PORT  1
#define SPI_MISO1_PIN   3
#define SPI_MOSI1_PORT  1
#define SPI_MOSI1_PIN   4

static bool initSPI = false; /**< Indica si se ha llamado a la funcion de inicializacion de la spi*/

void InitSPI(void)
{
	Chip_SSP_Init(LPC_SSP1);
	Chip_SCU_PinMuxSet(SPI_CLK0_PORT, SPI_CLK0_PIN, (SCU_MODE_PULLUP | SCU_MODE_FUNC0)); /* CLK0 */
	Chip_SCU_PinMuxSet(SPI_MISO1_PORT, SPI_MISO1_PIN, (SCU_MODE_PULLUP | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS | SCU_MODE_FUNC5)); /* MISO1 */
	Chip_SCU_PinMuxSet(SPI_MOSI1_PORT, SPI_MOSI1_PIN, (SCU_MODE_PULLUP | SCU_MODE_FUNC5)); /* MOSI1 */


	Chip_SSP_SetFormat(LPC_SSP1, SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_MODE1);
	Chip_SSP_Enable(LPC_SSP1);
	Chip_SSP_SetMaster(LPC_SSP1, 1);
	Chip_SSP_SetBitRate(LPC_SSP1, BIT_RATE);
	initSPI = true;
}

void DeinitSPI(void)
{
	Chip_SSP_DeInit(LPC_SSP1);
}

void TransferenciaBufferSPI(uint8_t *tx_buf, uint8_t *rx_buf, uint8_t longitud)
{
	if(!initSPI)
		InitSPI();

	Chip_SSP_DATA_SETUP_T ssp_xf;
	ssp_xf.length = longitud;
	ssp_xf.rx_cnt = 0;
	ssp_xf.rx_data = rx_buf;
	ssp_xf.tx_cnt = 0;
	ssp_xf.tx_data = tx_buf;

	Chip_SSP_RWFrames_Blocking(LPC_SSP1, &ssp_xf);
}

uint8_t TransferenciaRapidaSPI(uint8_t tx)
{
	if(!initSPI)
		InitSPI();
	uint8_t in;
	LPC_SSP1->DR = tx & 0xFFFF;
	while (LPC_SSP1->SR & 0x10 ) { ; }
	in = LPC_SSP1->DR;
	return in;

}

/** @}*/
