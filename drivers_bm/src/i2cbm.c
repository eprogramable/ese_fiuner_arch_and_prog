/*
 * i2cbm.c
 *
 *  Created on: 30 nov. 2018
 *      Author: esteban
 */

#include "i2cbm.h"

void I2CInit()
{
	/* Configuracion de las lineas de SDA y SCL de la placa*/
    Chip_SCU_I2C0PinConfig( I2C0_STANDARD_FAST_MODE );
    /* Inicializacion del periferico*/
    Chip_I2C_Init( I2C_PORT );
    /* Seleccion de velocidad del bus*/
    Chip_I2C_SetClockRate( I2C_PORT, I2C_DATA_RATE );
    /* Configuracion para que los eventos se resuelvan por polling
    // (la otra opcion es por interrupcion)*/
    Chip_I2C_SetMasterEventHandler( I2C_PORT, Chip_I2C_EventHandlerPolling );
    // ToDo: Verificar si se inicializó correctamente
    //gy271_i2c_inited = 1;
}

uint8_t I2CWrite(uint16_t address, uint8_t* data, uint16_t data_size)
{
	uint8_t res = TRUE;
    I2CM_XFER_T i2cData;

    // Prepare the i2cData register
    i2cData.slaveAddr = address;
    i2cData.options   = 0;
    i2cData.status    = 0;
    i2cData.txBuff    = data;
    i2cData.txSz      = data_size;
    i2cData.rxBuff    = 0;
    i2cData.rxSz      = 0;

    /* Send the i2c data */
    if( Chip_I2CM_XferBlocking( LPC_I2C0, &i2cData ) == 0 ){
       res = FALSE;
    }

    return res;
}

uint8_t I2CRead(  uint16_t  i2cSlaveAddress,
				  uint8_t* dataToReadBuffer,
				  uint16_t dataToReadBufferSize,
				  uint8_t* receiveDataBuffer,
				  uint16_t receiveDataBufferSize)
{
	uint8_t res = TRUE;

      I2CM_XFER_T i2cData;

      i2cData.slaveAddr = i2cSlaveAddress;
      i2cData.options   = 0;
      i2cData.status    = 0;
      i2cData.txBuff    = dataToReadBuffer;
      i2cData.txSz      = dataToReadBufferSize;
      i2cData.rxBuff    = receiveDataBuffer;
      i2cData.rxSz      = receiveDataBufferSize;

      if( Chip_I2CM_XferBlocking( LPC_I2C0, &i2cData ) == 0 ) {
         res =  FALSE;
      }

      return res;
}
