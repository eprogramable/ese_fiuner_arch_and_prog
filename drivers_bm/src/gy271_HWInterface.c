/*
 * gy271_HWInterface.c
 *
 *  Created on: 6 dic. 2018
 *      Author: esteban
 */
#include "gy271_HWInterface.h"
#include "chip.h"
#include "i2cbm.h"

enum{
   /* EDU-CIAA-NXP */

   // P1 header
   T_FIL1,    T_COL2,    T_COL0,    T_FIL2,      T_FIL3,  T_FIL0,     T_COL1,
   CAN_TD,    CAN_RD,    RS232_TXD, RS232_RXD,

   // P2 header
   GPIO8,     GPIO7,     GPIO5,     GPIO3,       GPIO1,
   LCD1,      LCD2,      LCD3,      LCDRS,       LCD4,
   SPI_MISO,
   ENET_TXD1, ENET_TXD0, ENET_MDIO, ENET_CRS_DV, ENET_MDC, ENET_TXEN, ENET_RXD1,
   GPIO6,     GPIO4,     GPIO2,     GPIO0,
   LCDEN,
   SPI_MOSI,
   ENET_RXD0,

   // Switches
   // 36   37     38     39
   TEC1,  TEC2,  TEC3,  TEC4,

   // Leds
   // 40   41     42     43     44     45
   LEDR,  LEDG,  LEDB,  LED1,  LED2,  LED3,

   /* CIAA-NXP */
 /* 46     47     48     49     50     51     52     53 */
   DI0,   DI1,   DI2,   DI3,   DI4,   DI5,   DI6,   DI7,
 /* 54     55     56     57     58     59     60     61 */
   DO0,   DO1,   DO2,   DO3,   DO4,   DO5,   DO6,   DO7
};


#define GY271_CONFIG_REG_A	    0x00  /*!< Configuration Register A   -> RW */
#define GY271_CONFIG_REG_B	    0x01  /*!< Configuration Register B   -> RW */
#define GY271_MODE_REG		    0x02  /*!< Mode Register		      -> RW */
#define GY271_DOUT_X_MSB_REG	0x03  /*!< Data Output X MSB Register -> R- */
#define GY271_DOUT_X_LSB_REG	0x04  /*!< Data Output X LSB Register -> R- */
#define GY271_DOUT_Z_MSB_REG	0x05  /*!< Data Output Y MSB Register -> R- */
#define GY271_DOUT_Z_LSB_REG	0x06  /*!< Data Output Y LSB Register -> R- */
#define GY271_DOUT_Y_MSB_REG	0x07  /*!< Data Output Z MSB Register -> R- */
#define GY271_DOUT_Y_LSB_REG	0x08  /*!< Data Output Z LSB Register -> R- */
#define GY271_STATUS_REG	    0x09  /*!< Status Register 	          -> R- */
#define GY271_ID_REG_A	    	0x0A  /*!< Identification Register A  -> R- */
#define GY271_ID_REG_B		    0x0B  /*!< Identification Register B  -> R- */
#define GY271_ID_REG_C		    0x0C  /*!< Identification Register C  -> R- */

#define GY271_WRITE 			0x3C
#define GY271_READ				0x3D


#define GPIO0_IRQ	0
#define GPIO1_IRQ	1
#define GPIO2_IRQ	2
#define GPIO3_IRQ	3
#define GPIO4_IRQ 	4
#define GPIO5_IRQ	5
#define GPIO6_IRQ 	6
#define GPIO7_IRQ 	7
#define GPIO8_IRQ	8

uint8_t gy271_i2c_inited = 0;



void GPIO0_IRQHandler()
{
	Chip_PININT_ClearIntStatus (LPC_GPIO_PIN_INT , PININTCH (GPIO0_IRQ));
	NVIC_ClearPendingIRQ (PIN_INT0_IRQn);
	NVIC_EnableIRQ(PIN_INT0_IRQn);
	GY271ReadDevice(&GY271_devices[0]);
}

void GPIO1_IRQHandler()
{
	Chip_PININT_ClearIntStatus (LPC_GPIO_PIN_INT , PININTCH (GPIO1_IRQ));
	NVIC_ClearPendingIRQ (PIN_INT1_IRQn);
	NVIC_EnableIRQ(PIN_INT1_IRQn);
	GY271ReadDevice(&GY271_devices[1]);

}
void GPIO2_IRQHandler()
{
	Chip_PININT_ClearIntStatus (LPC_GPIO_PIN_INT , PININTCH (GPIO2_IRQ));
	NVIC_ClearPendingIRQ (PIN_INT2_IRQn);
	NVIC_EnableIRQ(PIN_INT2_IRQn);
	GY271ReadDevice(&GY271_devices[2]);

}
void GPIO3_IRQHandler()
{
	Chip_PININT_ClearIntStatus (LPC_GPIO_PIN_INT , PININTCH (GPIO3_IRQ));
	NVIC_ClearPendingIRQ (PIN_INT3_IRQn);
	NVIC_EnableIRQ(PIN_INT3_IRQn);
	GY271ReadDevice(&GY271_devices[3]);
}


void GY271HWIInitPorts(GY271_magnetometer_t device){
	// preparing port configuration
	port_t 			DRDY_GPIO_port;
	switch(device.config.GPIO_DRDY)
	{
	case GPIO0:
		DRDY_GPIO_port.gpio_number = GPIO0_IRQ;
		DRDY_GPIO_port.scu_pin = 1;
		DRDY_GPIO_port.scu_port = 6;
		DRDY_GPIO_port.gpio_port = 3;
		DRDY_GPIO_port.gpio_pin = 0;
		DRDY_GPIO_port.config_mode = 0;
		DRDY_GPIO_port.IRQNumber = 0;
		DRDY_GPIO_port.IRQHandler = GPIO0_IRQHandler;
		break;
	case GPIO1:
		DRDY_GPIO_port.gpio_number = GPIO1_IRQ;
		DRDY_GPIO_port.scu_pin = 4;
		DRDY_GPIO_port.scu_port = 6;
		DRDY_GPIO_port.gpio_port = 3;
		DRDY_GPIO_port.gpio_pin = 3;
		DRDY_GPIO_port.config_mode = 0;
		DRDY_GPIO_port.IRQNumber = 1;
		DRDY_GPIO_port.IRQHandler = GPIO1_IRQHandler;
		break;
	case GPIO2:
		DRDY_GPIO_port.gpio_number = GPIO2_IRQ;
		DRDY_GPIO_port.scu_pin = 5;
		DRDY_GPIO_port.scu_port = 6;
		DRDY_GPIO_port.gpio_port = 3;
		DRDY_GPIO_port.gpio_pin = 4;
		DRDY_GPIO_port.config_mode = 0;
		DRDY_GPIO_port.IRQNumber = 2;
		DRDY_GPIO_port.IRQHandler = GPIO2_IRQHandler;
		break;
	case GPIO3:
		DRDY_GPIO_port.gpio_number = GPIO3_IRQ;
		DRDY_GPIO_port.scu_port = 6;
		DRDY_GPIO_port.scu_pin = 7;
		DRDY_GPIO_port.gpio_port = 5;
		DRDY_GPIO_port.gpio_pin = 15;
		DRDY_GPIO_port.config_mode = 4;
		DRDY_GPIO_port.IRQNumber = 3;
		DRDY_GPIO_port.IRQHandler = GPIO3_IRQHandler;
		break;
	}
	// writing actual port configuration.
	Chip_GPIO_Init(LPC_GPIO_PORT); // does nothing
	Chip_SCU_PinMuxSet(DRDY_GPIO_port.scu_port,DRDY_GPIO_port.scu_pin, (SCU_MODE_PULLUP | SCU_MODE_INBUFF_EN | DRDY_GPIO_port.config_mode));
	Chip_GPIO_SetPinDIRInput (LPC_GPIO_PORT, DRDY_GPIO_port.gpio_port, DRDY_GPIO_port.gpio_pin);
    Chip_SCU_GPIOIntPinSel(DRDY_GPIO_port.IRQNumber, DRDY_GPIO_port.gpio_port, DRDY_GPIO_port.gpio_pin);

	// Interruptions setup
	Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH(DRDY_GPIO_port.IRQNumber));
	Chip_PININT_EnableIntLow(LPC_GPIO_PIN_INT, PININTCH(DRDY_GPIO_port.IRQNumber));

	switch(device.config.GPIO_DRDY)
	{
	case GPIO0:
		 NVIC_ClearPendingIRQ(PIN_INT0_IRQn);
		 NVIC_EnableIRQ(PIN_INT0_IRQn);
		 break;
	case GPIO1:
		 NVIC_ClearPendingIRQ(PIN_INT1_IRQn);
		 NVIC_EnableIRQ(PIN_INT1_IRQn);
		 break;
	case GPIO2:
		 NVIC_ClearPendingIRQ(PIN_INT2_IRQn);
		 NVIC_EnableIRQ(PIN_INT2_IRQn);
		 break;
	case GPIO3:
		 NVIC_ClearPendingIRQ(PIN_INT3_IRQn);
		 NVIC_EnableIRQ(PIN_INT3_IRQn);
		 break;
	}
	I2CInit();
}


uint8_t GY271HWIReadDevice(GY271_magnetometer_t * GY271_dev)
{
	uint8_t read_res, out,reg_add;

	uint8_t x_MSB, x_LSB;
	uint8_t y_MSB, y_LSB;
	uint8_t z_MSB, z_LSB;

	reg_add = GY271_DOUT_X_MSB_REG;
	out = I2CRead(GY271_dev->base_address, &reg_add , sizeof(reg_add), &x_MSB, sizeof(x_MSB));
	reg_add = GY271_DOUT_X_LSB_REG;
	read_res = I2CRead(GY271_dev->base_address, &reg_add , sizeof(reg_add ), &x_LSB, sizeof(x_LSB));
	GY271_dev->x_read = (x_MSB << 8 ) | x_LSB;
	out = (out << 1 ) | read_res;

	reg_add = GY271_DOUT_Z_MSB_REG;
	read_res = I2CRead(GY271_dev->base_address, &reg_add , sizeof(reg_add), &z_MSB, sizeof(z_MSB));
	out = (out << 1 ) | read_res;
	reg_add = GY271_DOUT_Z_LSB_REG;
	read_res = I2CRead(GY271_dev->base_address, &reg_add , sizeof(reg_add ), &z_LSB, sizeof(z_LSB));
	out = (out << 1 ) | read_res;
	GY271_dev->z_read = (z_MSB << 8) | z_LSB;

	reg_add = GY271_DOUT_Y_MSB_REG;
	read_res = I2CRead(GY271_dev->base_address, &reg_add , sizeof(reg_add), &y_MSB, sizeof(y_MSB));
	out = (out << 1 ) | read_res;
	reg_add = GY271_DOUT_Y_LSB_REG;
	read_res = I2CRead(GY271_dev->base_address, &reg_add , sizeof(reg_add ), &y_LSB, sizeof(y_LSB));
	out = (out << 1 ) | read_res;
	GY271_dev->y_read = (y_MSB << 8) | y_LSB;

	return out;
}

uint8_t gy271HWIConfigDevice(GY271_magnetometer_t GY271_dev)
{
	uint8_t res_write,out, d_out[2];
	out = 0;
	if(!gy271_i2c_inited)
	{
		I2CInit();
		gy271_i2c_inited = 1;
	}
	GY271HWIInitPorts(GY271_dev);

	/*Register A*/
	d_out[0] = GY271_CONFIG_REG_A;
	d_out[1] = (GY271_dev.config.flow << 0);
	d_out[1] |= (GY271_dev.config.rate << 2);
	d_out[1] |= (GY271_dev.config.samples << 5);
	out = I2CWrite(GY271_dev.base_address, d_out, sizeof(d_out));

	/*Register B*/
	d_out[0] = GY271_CONFIG_REG_B;
	d_out[1] = 0;
	d_out[1] |= (GY271_dev.config.gain <<5);
	res_write = I2CWrite(GY271_dev.base_address, d_out, sizeof(d_out));
	out = (out<<1) | res_write;
	/* Mode register*/
	d_out[0] = GY271_MODE_REG;
	d_out[1] = 0;
	if(I2C_DATA_RATE == 400000)
	{
		d_out[1] |= (1 << 7);
	}

	d_out[1] |= (GY271_dev.config.mode <<0);
	res_write = I2CWrite(GY271_dev.base_address, d_out, sizeof(d_out));
	out = (out<<1) | res_write;
	return out;
}
