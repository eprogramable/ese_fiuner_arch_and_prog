 /*
 Copyright 2018 Joaquin Furios
 
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
 
 /*
 * Initials     Name
 * ---------------------------
 *  JF          Joaquin Furios
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 2018/12/04 v1.0  first full version
 * 2018/11/20 v0.1.201118 Initial version
 */

/*==================[inclusions]=============================================*/

#include "chip.h"
#include "bt_hc05.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/
uart_t rs232 = UART_RS232;
uart_t usb   = UART_USB;

/*==================[internal functions declaration]=========================*/
void ItrUartRs232();
void ItrUartUsb();
/*==================[internal data definition]===============================*/


/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/


/*==================[external functions definition]==========================*/
void btInit(){
    InitUart(rs232);
    InitUart(usb);
}


void btActivateTransparentModUsb(){
    ActivarIntUart(rs232,ItrUartRs232);
    ActivarIntUart(usb,ItrUartUsb);
}

void ItrUartRs232(){
    uint8_t *byte;

    if(LeerByteUart(rs232,&byte))
        EnviarByteUart(usb,&byte);
    
}

void ItrUartUsb(){
    uint8_t *byte;

    if(LeerByteUart(usb,&byte)){
        
        EnviarByteUart(usb,&byte);
        EnviarByteUart(rs232,&byte);
    }
}


void btSendByte (uint8_t *byte){
    EnviarByteUart(rs232, byte);
}

void btSendString (uint8_t *msg){
    EnviarCadenaUart(rs232,msg);
}

void btReadByte (uint8_t *byte){
    LeerByteUart(rs232,byte);
}

void btReadString (uint8_t *msg){
    LeerCadenaUart(rs232,msg);
}

void btChangeName (uint8_t *name){
        
    uint8_t ATcode[] = {"AT+NAME="};
 
    EnviarCadenaUart(rs232,&ATcode);
    EnviarCadenaUart(rs232,name);
    EnviarCadenaUart(rs232,"\r\n");
}

void btChangePass (uint8_t *pass){
    
    uint8_t ATcode[] = {"AT+PSWD="};
 
    EnviarCadenaUart(rs232,&ATcode);
    EnviarCadenaUart(rs232,pass);
    EnviarCadenaUart(rs232,"\r\n");

}

void btChangeBaudRate (uint8_t *baud){
    
    uint8_t ATcode[] = {"AT+UART="};
 
    EnviarCadenaUart(rs232,&ATcode);
    EnviarCadenaUart(rs232,baud);
    EnviarCadenaUart(rs232,"\r\n");
}

void btReadName (uint8_t *name){
    
    uint8_t ATcode[] = {"AT+NAME?\r\n"};
    EnviarCadenaUart(rs232,&ATcode);
    while(EstadoRecepcionUart(rs232)==0);
    LeerCadenaUart(rs232,&name);
}

void btReadPass (uint8_t *pass){
    
    uint8_t ATcode[] = {"AT+PSWD?\r\n"};
    EnviarCadenaUart(rs232,&ATcode);
    while(EstadoRecepcionUart(rs232)==0);
    LeerCadenaUart(rs232,&pass);
}

void btReadBaudRate (uint8_t *baud){
    uint8_t ATcode[] = {"AT+UART?\r\n"};
    EnviarCadenaUart(rs232,&ATcode);
    while(EstadoRecepcionUart(rs232)==0);
    LeerCadenaUart(rs232,&baud);
    
}

/*==================[end of file]============================================*/