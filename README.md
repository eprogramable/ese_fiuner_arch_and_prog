
![fiuner|829x271,30%](fiuner.png)
#Career: Embedded Systems Specialization 
Course: Embedded Systems Architecture and Programming. 
-----------

Initial Firmware/Hardware:[EDU-CIAA](www.proyecto-ciaa.com.ar/) Flavour: NXP (LPC4337JBD144)

*  [Repositorio oficial del proyecto CIAA.](https://github.com/ciaa)



**Devices Drivers List:**

* Oled Display - SH1106
* Bluetooth BLE - ESP32
* Compass - GY271
* IMU - MPU9250 
* NFC - PN532
*  RF/RS232 bridge - APC230
* 3-axis Accelerometer - MMA8451
* TFT color 2.2" - ILI9341
* RS232 & RS485 uart
* Temperature and Humidity - STH71x 
* Digital RGB LED - WS2612B
* RF - NRF24L01 
* Biopotentials Acquisition - ADS1299
* Steper Motor Driver - DRV8825
* BlueTooth - HC05
 

** [Device Datasheets](https://drive.google.com/drive/folders/1_NJAfac5yp1gTr2hD6mrd9aooWTOTRYA?usp=sharing) **

**Authors:** 

 *  Juan Manuel Reta (jmreta@ingenieria.uner.edu.ar)
 *  Eduardo Filomena (efilomena@ingenieria.uner.edu.ar)
 *  Gonzalo Cuenca (gcuenca@ingenieria.uner.edu.ar)
 *  Juan Ignacio Cerrudo (jcerrudo@ingenieria.uner.edu.ar)
 *  Albano Peñalva (apenialva@ingeneiria.uner.edu.ar)
 *  Sebastián Mateos (sebastianantoniomateos@gmail.com)
 *  Diana Vertiz Del Valle (dvertizdelvalle@ingenieria.uner.edu.ar)
 *  Christian Halter (halter.cd@gmail.com)
 *  Flavio	Cavallo (cavallo.f@gmail.com)
 *  Leandro Escher (lgescher@ingenieria.uner.edu.ar)
 *  Denis Jorge Genero (denisgenero@live.com.ar)
 *  Germán Hachmann (hachmanng@fcal.uner.edu.ar)
 *  Juan Carlos Neville (jcneville@gmail.com)
 *  Esteban Osella (eosella@ingenieria.uner.edu.ar)
 *  Maximiliano Leikan (mleikan@ingenieria.uner.edu.ar)
 *  Adrián	Lencina (adrianlencina@frp.utn.edu.ar)
 *  Sebastián Vicario (sebastian_vicario@hotmail.com)
 *  Franco	Cipriani ()
 *  Julián	Botello (jlnbotello@gmail.com )
 *  Osvaldo Zanet (tato_713@hotmail.com)
 *  Joaquin Furios (joaquinfurios@gmail.com)





